package com.tructuanguy.elearning.base


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.extension.hideKeyboard
import java.io.Serializable


abstract class BaseFragment : Fragment() {
    private var activity : BaseActivity<*>? = null
    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        view.setOnTouchListener(OnTouchListener { v, event -> // Ẩn bàn phím nếu đang hiển thị
//            v.hideKeyboard(requireActivity())
//            true
//        })
        activity = requireActivity() as BaseActivity<*>
        init()
        initData()
        initAction()
    }

    fun createBundle(key: String, data: Any?): Bundle {
        val bundle = Bundle()
        bundle.putSerializable(key, data as Serializable?)
        return bundle
    }

    fun openFragment(
        fragmentClazz: Class<*>,
        args: Bundle?,
        addBackStack: Boolean,
        id: Int = R.id.fr_app
    ) {
        activity?.openFragment(fragmentClazz, args, addBackStack, id)
    }

    fun toast(text: String) {
        Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show()
    }
    fun onBackPressed() {
        activity?.onBackPressedDispatcher?.onBackPressed()
    }

    abstract fun init()
    abstract fun initData()
    abstract fun initAction()

    fun showLoadingDialog() = activity?.showLoadingDialog()

    fun hideLoadingDialog() = activity?.hideLoadingDialog()


}