package com.tructuanguy.elearning.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {
    val repository = Repository()
    val isLoading: MutableLiveData<Boolean> = MutableLiveData(false)
}