package com.tructuanguy.elearning.common

enum class EditTextError {
    PASSWORD_LENGTH,
    EMAIL_FORMAT,
    EMAIL_EMPTY,
    PASSWORD_EMPTY,
    USERNAME_EMPTY
}