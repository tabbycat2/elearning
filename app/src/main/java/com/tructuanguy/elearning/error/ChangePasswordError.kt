package com.tructuanguy.elearning.error

enum class ChangePasswordError {
    SAME_OLD_NEW_PASSWORD,
    DIFF_NEW_CONFIRM_PASSWORD,
    DIFF_OLD_USER_PASSWORD
}