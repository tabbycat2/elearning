package com.tructuanguy.elearning

import com.tructuanguy.elearning.constants.FirebaseConstants.INFO_NOT_SET
import java.io.Serializable

data class User(
    var id: String = INFO_NOT_SET,
    var userName: String = INFO_NOT_SET,
    var email: String = INFO_NOT_SET,
    var password: String = INFO_NOT_SET,
    var image: String = INFO_NOT_SET,
    var exp: Int = 0
): Serializable
