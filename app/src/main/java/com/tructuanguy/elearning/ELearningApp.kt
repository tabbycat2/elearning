package com.tructuanguy.elearning

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ELearningApp : Application() {
}