package com.tructuanguy.elearning.db.domain.usecases.vocabulary

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import com.tructuanguy.elearning.db.entities.Word
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetWordByTopicId @Inject constructor(
    private val dbRepository: AppDBRepository
) {
    suspend operator fun invoke(id: Int): Flow<DataState<List<Word>>> = dbRepository.getWordByTopicId(id)
}