package com.tructuanguy.elearning.db.domain.usecases.exercise

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import com.tructuanguy.elearning.db.entities.GeneralExeQuestion
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetGeneralExeQuestion @Inject constructor(
    private val appDBRepository: AppDBRepository
) {
    suspend operator fun invoke(start: Int, end: Int): Flow<DataState<List<GeneralExeQuestion>>> = appDBRepository.getGeneralExeQuestion(start, end)
}