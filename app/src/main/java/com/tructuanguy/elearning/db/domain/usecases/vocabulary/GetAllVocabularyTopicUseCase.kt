package com.tructuanguy.elearning.db.domain.usecases.vocabulary

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import com.tructuanguy.elearning.feature.main.model.Topic
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllVocabularyTopicUseCase @Inject constructor(
    private val appDBRepository: AppDBRepository
) {
    suspend operator fun invoke(): Flow<DataState<List<Topic>>> = appDBRepository.getVocabularyTopic()
}