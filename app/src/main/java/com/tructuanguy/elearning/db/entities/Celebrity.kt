package com.tructuanguy.elearning.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Celebrity (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var words: String? = null,
    var phonetic: String? = null,
    var type: String? = null,
    var means: String? = null,
    var cele_eng: String? = null,
    var cele_vn: String? = null,
    var time_access: String? = null,
    var author: String? = null
)