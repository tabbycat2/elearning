package com.tructuanguy.elearning.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Word(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val topicId: Int,
    val word: String,
    val mean: String,
    val image: String,
    var spell_uk: String? = null,
    var spell_us: String? = null,
    var example_eng: String? = null,
    var example_vn: String? = null,
    var explain_eng: String? = null,
    var explain_vn: String ? = null,
    var url: String? = null
) : Serializable
