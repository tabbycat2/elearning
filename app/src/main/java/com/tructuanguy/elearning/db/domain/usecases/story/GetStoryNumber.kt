package com.tructuanguy.elearning.db.domain.usecases.story

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetStoryNumber @Inject constructor(
    private val dbRepository: AppDBRepository
) {
    suspend operator fun invoke(): Flow<DataState<List<Int>>> = dbRepository.getStoryNumber()
}