package com.tructuanguy.elearning.db.domain.usecases.quote

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import com.tructuanguy.elearning.db.entities.Celebrity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllQuotes @Inject constructor(
    private val dbRepository: AppDBRepository
) {
    suspend operator fun invoke(): Flow<DataState<List<Celebrity>>> = dbRepository.getQuotes()
}