package com.tructuanguy.elearning.db.domain.data

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.AppDB
import com.tructuanguy.elearning.db.entities.UserLocal
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import com.tructuanguy.elearning.db.entities.Celebrity
import com.tructuanguy.elearning.db.entities.ExamQuestion
import com.tructuanguy.elearning.db.entities.GeneralExeQuestion
import com.tructuanguy.elearning.db.entities.Grammar
import com.tructuanguy.elearning.db.entities.Story
import com.tructuanguy.elearning.db.entities.Word
import com.tructuanguy.elearning.feature.main.model.Topic
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class AppDBRepositoryImpl @Inject constructor(
    private val db: AppDB
): AppDBRepository {

    override suspend fun insertUserLocal(user: UserLocal): Flow<DataState<Boolean>> {
        return flow {
            emit(DataState.Loading())
            try {
                db.dao.insertUserLocal(user)
                emit(DataState.Success(true))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getAllUserLocal(): Flow<DataState<List<UserLocal>>> {
        return flow {
            emit(DataState.Loading())
            try {
                val list = db.dao.getAllUserLocal()
                emit(DataState.Success(list))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun isSavedLocalUser(email: String): Flow<DataState<Boolean>> {
        return flow {
            emit(DataState.Loading())
            try {
                val counter = db.dao.isSavedLocalAcc(email)
                if(counter > 0) emit(DataState.Success(true)) else emit(DataState.Success(false))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun deleteUserLocal(id: Int): Flow<DataState<Boolean>> {
        return flow {
            emit(DataState.Loading())
            try {
                db.dao.deleteUserLocal(id)
                emit(DataState.Success(true))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getVocabularyTopic(): Flow<DataState<List<Topic>>> {
        return flow {
            emit(DataState.Loading())
            try {
                val list = db.dao.getVocabularyTopics()
                emit(DataState.Success(list))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getParentGrammar(): Flow<DataState<List<Grammar>>> {
        return flow {
            emit(DataState.Loading())
            try {
                emit(DataState.Success(db.dao.getParentGrammar()))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getChildGrammar(): Flow<DataState<List<Grammar>>> {
        return flow {
            emit(DataState.Loading())
            try {
                emit(DataState.Success(db.dao.getChildGrammar()))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getQuotes(): Flow<DataState<List<Celebrity>>> {
        return flow {
            emit(DataState.Loading())
            try {
                emit(DataState.Success(db.dao.getQuotes()))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getStory(id: Int): Flow<DataState<List<Story>>> {
        return flow {
            emit(DataState.Loading())
            try {
                emit(DataState.Success(db.dao.getStory(id)))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getStoryNumber(): Flow<DataState<List<Int>>> {
        return flow {
            emit(DataState.Loading())
            try {
                emit(DataState.Success(db.dao.getStoryNumber()))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getAllWords(): Flow<DataState<List<Word>>> {
        return flow {
            emit(DataState.Loading())
            try {
                emit(DataState.Success(db.dao.getAllWord()))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getGeneralExeQuestion(start: Int, end: Int): Flow<DataState<List<GeneralExeQuestion>>> {
        return flow {
            emit(DataState.Loading())
            try {
                emit(DataState.Success(db.dao.getGeneralExeQuestion(start, end)))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getExamQuestion(
        start: Int,
        end: Int
    ): Flow<DataState<List<ExamQuestion>>> {
        return flow {
            emit(DataState.Loading())
            try {
                emit(DataState.Success(db.dao.getExamQuestion(start, end)))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getWordByTopicId(id: Int): Flow<DataState<List<Word>>> {
        return flow {
            emit(DataState.Loading())
            try {
                emit(DataState.Success(db.dao.getWord(id)))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

}