package com.tructuanguy.elearning.db.domain.usecases.word

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import com.tructuanguy.elearning.db.entities.Word
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllWord @Inject constructor(
    private val dbRepository: AppDBRepository
) {
    suspend operator fun invoke(): Flow<DataState<List<Word>>> = dbRepository.getAllWords()
}