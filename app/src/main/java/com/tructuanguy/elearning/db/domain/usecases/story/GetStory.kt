package com.tructuanguy.elearning.db.domain.usecases.story

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import com.tructuanguy.elearning.db.entities.Story
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetStory @Inject constructor(
    private val dbRepository: AppDBRepository
) {
    suspend operator fun invoke(id: Int): Flow<DataState<List<Story>>> = dbRepository.getStory(id)
}