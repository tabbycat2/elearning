package com.tructuanguy.elearning.db.entities

import java.io.Serializable

data class Lesson (
    val id: Int,
    val name: String
): Serializable