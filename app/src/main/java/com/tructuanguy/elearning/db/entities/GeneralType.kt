package com.tructuanguy.elearning.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class GeneralType (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var test_id: Int? = null,
    var title: String? = null,
    var paragraph: String? = null
)