package com.tructuanguy.elearning.db.domain.usecases.exercise

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import com.tructuanguy.elearning.db.entities.ExamQuestion
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetExamQuestion @Inject constructor(
    private val appDBRepository: AppDBRepository
) {
    suspend operator fun invoke(start: Int, end: Int): Flow<DataState<List<ExamQuestion>>> = appDBRepository.getExamQuestion(start, end)
}