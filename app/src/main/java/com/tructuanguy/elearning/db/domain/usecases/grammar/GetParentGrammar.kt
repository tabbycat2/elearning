package com.tructuanguy.elearning.db.domain.usecases.grammar

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import com.tructuanguy.elearning.db.entities.Grammar
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetParentGrammar @Inject constructor(
    private val dbRepository: AppDBRepository
) {
    suspend operator fun invoke(): Flow<DataState<List<Grammar>>> = dbRepository.getParentGrammar()
}