package com.tructuanguy.elearning.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class GeneralExeQuestion (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var question_id: Int? = null,
    var content: String? = null,
    var answers: String? = null,
    var t_key: Int? = null,
    var t_explain: String? = null,
    var t_answer: Int? = null
)