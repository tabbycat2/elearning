package com.tructuanguy.elearning.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tructuanguy.elearning.db.entities.Celebrity
import com.tructuanguy.elearning.db.entities.GeneralExeQuestion
import com.tructuanguy.elearning.db.entities.GeneralType
import com.tructuanguy.elearning.db.entities.Grammar
import com.tructuanguy.elearning.db.entities.Story
import com.tructuanguy.elearning.db.entities.UserLocal
import com.tructuanguy.elearning.db.entities.Word
import com.tructuanguy.elearning.feature.main.model.Topic


@Database(
    entities = [UserLocal::class, Topic::class, Grammar::class, Celebrity::class, Story::class, Word::class,
               GeneralType::class, GeneralExeQuestion::class],
    version = 1
)
abstract class AppDB : RoomDatabase() {

    abstract val dao: AppDao

}