package com.tructuanguy.elearning.db.di

import com.tructuanguy.elearning.db.AppDB
import com.tructuanguy.elearning.db.domain.data.AppDBRepositoryImpl
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppDBModule {

    @Singleton
    @Provides
    fun provideAppDBRepository(db: AppDB): AppDBRepository {
        return AppDBRepositoryImpl(db)
    }

}