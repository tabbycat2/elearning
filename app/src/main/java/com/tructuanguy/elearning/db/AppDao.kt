package com.tructuanguy.elearning.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tructuanguy.elearning.db.entities.Celebrity
import com.tructuanguy.elearning.db.entities.ExamQuestion
import com.tructuanguy.elearning.db.entities.GeneralExeQuestion
import com.tructuanguy.elearning.db.entities.Grammar
import com.tructuanguy.elearning.db.entities.Story
import com.tructuanguy.elearning.db.entities.UserLocal
import com.tructuanguy.elearning.db.entities.Word
import com.tructuanguy.elearning.feature.main.model.Topic

@Dao
interface AppDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserLocal(user: UserLocal)

    @Query("select * from UserLocal")
    fun getAllUserLocal(): List<UserLocal>

    @Query("select count(*) from UserLocal where email = :email")
    fun isSavedLocalAcc(email: String): Int

    @Query("delete from UserLocal where id = :id")
    fun deleteUserLocal(id: Int)

    @Query("select * from Topic")
    fun getVocabularyTopics(): List<Topic>

    @Query("select * from Grammar where parent_id = 0")
    fun getParentGrammar(): List<Grammar>

    @Query("select * from Grammar where parent_id = 1")
    fun getChildGrammar(): List<Grammar>

    @Query("select * from Celebrity")
    fun getQuotes(): List<Celebrity>

    @Query("select * from Story where lesson_id = :id")
    fun getStory(id: Int): List<Story>

    @Query("select count(*) from Story group by lesson_id")
    fun getStoryNumber(): List<Int>

    @Query("select * from Word")
    fun getAllWord(): List<Word>

    @Query("select * from GeneralExeQuestion where id between :start and :end")
    fun getGeneralExeQuestion(start: Int, end: Int): List<GeneralExeQuestion>

    @Query("select GeneralExeQuestion.id, title, paragraph, content, answers, t_key, t_explain, t_answer from GeneralType, GeneralExeQuestion where GeneralType.id = GeneralExeQuestion.question_id \n" +
            "and GeneralType.id BETWEEN :start and :end")
    fun getExamQuestion(start: Int, end: Int): List<ExamQuestion>

    @Query("select * from Word where topicId = :id")
    fun getWord(id: Int): List<Word>


}