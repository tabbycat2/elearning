package com.tructuanguy.elearning.db.domain.repository

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.entities.Celebrity
import com.tructuanguy.elearning.db.entities.ExamQuestion
import com.tructuanguy.elearning.db.entities.GeneralExeQuestion
import com.tructuanguy.elearning.db.entities.Grammar
import com.tructuanguy.elearning.db.entities.Story
import com.tructuanguy.elearning.db.entities.UserLocal
import com.tructuanguy.elearning.db.entities.Word
import com.tructuanguy.elearning.feature.main.model.Topic
import kotlinx.coroutines.flow.Flow

interface AppDBRepository {

    suspend fun insertUserLocal(user: UserLocal): Flow<DataState<Boolean>>

    suspend fun getAllUserLocal(): Flow<DataState<List<UserLocal>>>

    suspend fun isSavedLocalUser(email: String): Flow<DataState<Boolean>>

    suspend fun deleteUserLocal(id: Int): Flow<DataState<Boolean>>

    suspend fun getVocabularyTopic(): Flow<DataState<List<Topic>>>

    suspend fun getParentGrammar(): Flow<DataState<List<Grammar>>>

    suspend fun getChildGrammar(): Flow<DataState<List<Grammar>>>

    suspend fun getQuotes(): Flow<DataState<List<Celebrity>>>

    suspend fun getStory(id: Int): Flow<DataState<List<Story>>>

    suspend fun getStoryNumber(): Flow<DataState<List<Int>>>

    suspend fun getAllWords(): Flow<DataState<List<Word>>>

    suspend fun getGeneralExeQuestion(start: Int, end: Int): Flow<DataState<List<GeneralExeQuestion>>>

    suspend fun getExamQuestion(start: Int, end: Int): Flow<DataState<List<ExamQuestion>>>

    suspend fun getWordByTopicId(id: Int): Flow<DataState<List<Word>>>

}