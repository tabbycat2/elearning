package com.tructuanguy.elearning.db.entities

data class KeyWordQuote (
    val original: String,
    val pronunciation: String,
    val type: String,
    val mean: String
)