package com.tructuanguy.elearning.db.entities

data class ExamQuestion (
    val id: Int,
    var title: String? = null,
    var paragraph: String? = null,
    var content: String? = null,
    var answers: String? = null,
    var t_key: Int? = null,
    var t_explain: String? = null,
    var t_answer: Int = 0
)