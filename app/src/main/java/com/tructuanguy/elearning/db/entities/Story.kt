package com.tructuanguy.elearning.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Story (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var lesson_id: Int? = null,
    var title: String? = null,
    var description: String? = null,
    var content: String? = null,
    var mean: String? = null
): Serializable