package com.tructuanguy.elearning.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import javax.annotation.Nullable

@Entity
data class Grammar(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var name: String? = null,
    var mean: String? = null,
    var is_parent: Int? = null,
    var parent_id: Int? = null
): Serializable
