package com.tructuanguy.elearning.db.domain.usecases.user_local

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.entities.UserLocal
import com.tructuanguy.elearning.db.domain.repository.AppDBRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class InsertUserLocalUseCase @Inject constructor(
    private val appDBRepository: AppDBRepository
) {
    suspend operator fun invoke(user: UserLocal): Flow<DataState<Boolean>> = appDBRepository.insertUserLocal(user)
}