package com.tructuanguy.elearning.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.StringRes
import androidx.room.Room
import com.tructuanguy.elearning.constants.PrefsConstants
import com.tructuanguy.elearning.db.AppDB
import com.tructuanguy.elearning.db.AppDao
import com.tructuanguy.elearning.helper.PreferenceHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Inject
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application

    @Singleton
    @Provides
    fun providePreferenceHelper(mPrefs: SharedPreferences) = PreferenceHelper(mPrefs)

    @Singleton
    @Provides
    fun provideSharePreference(context: Context): SharedPreferences {
        return context.getSharedPreferences(PrefsConstants.MY_PREFS, Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideAppDB(@ApplicationContext context: Context): AppDB {
        return Room.databaseBuilder(
            context,
            AppDB::class.java,
            "elearning_v1.db"
        ).createFromAsset("db/elearning_v1.db").allowMainThreadQueries().build()
    }

    @Singleton
    @Provides
    fun provideAppDao(db: AppDB): AppDao {
        return db.dao
    }

    @Singleton
    class ResourceProvider @Inject constructor(
        @ApplicationContext private val context: Context
    ) {
        fun getString(@StringRes id: Int): String {
            return context.getString(id)
        }
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
    }


}