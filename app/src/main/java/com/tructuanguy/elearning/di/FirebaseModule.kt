package com.tructuanguy.elearning.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.tructuanguy.elearning.constants.FirebaseConstants.PROFILE_QUESTION_STATISTICAL
import com.tructuanguy.elearning.constants.FirebaseConstants.USER_COLLECTION
import com.tructuanguy.elearning.constants.FirebaseConstants.VOCABULARY_COLLECTION
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object FirebaseModule {

    @Singleton
    @Provides
    fun provideFirebaseAuthentication(): FirebaseAuth = FirebaseAuth.getInstance()

    @Singleton
    @Provides
    fun provideFireStore(): FirebaseFirestore = FirebaseFirestore.getInstance()

    @Singleton
    @Provides
    fun provideStorage(): FirebaseStorage = FirebaseStorage.getInstance()

    @UserCollection
    @Singleton
    @Provides
    fun provideUserCollection(
        firebaseFireStore: FirebaseFirestore
    ): CollectionReference {
        return firebaseFireStore.collection(USER_COLLECTION)
    }

    @VocabularyCollection
    @Singleton
    @Provides
    fun provideVocabularyCollection(
        firebaseFireStore: FirebaseFirestore
    ): CollectionReference {
        return firebaseFireStore.collection(VOCABULARY_COLLECTION)
    }


    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class UserCollection

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class VocabularyCollection


}