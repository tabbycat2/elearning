package com.tructuanguy.elearning.constants

object PrefsConstants {
    const val IS_FIRST_IN_APP = "in_first_in_app"
    const val ACCOUNT_SAVED = "account_saved"
    const val MY_PREFS = "my_preference"
    const val USER_EMAIL = "user_account"
    const val USER_PASSWORD = "user_password"
}