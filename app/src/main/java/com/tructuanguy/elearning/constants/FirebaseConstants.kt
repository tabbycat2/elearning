package com.tructuanguy.elearning.constants

object FirebaseConstants {
    const val USER = "user_bundle"
    const val PASSWORD = "password"
    const val USER_COLLECTION = "user"
    const val VOCABULARY_COLLECTION = "vocabulary"
    const val USERS_IMAGE_STORAGE = "user_image"
    const val PROFILE_QUESTION_STATISTICAL = "profile_question_statistical"
    const val INFO_NOT_SET = "info_not_set"
    const val USER_EMAIL = "user_email"
    const val USER_PASSWORD = "user_password"
    const val UID = "user_id"
    const val USER_EMAIL_PASSWORD = "user_email_password"
    const val QUESTION_1 = "question1"
    const val QUESTION_2 = "question2"
    const val QUESTION_3 = "question3"
    const val QUESTION_4 = "question4"
    const val ANSWER_1 = "answer1"
    const val ANSWER_2 = "answer2"
    const val ANSWER_3 = "answer3"
}