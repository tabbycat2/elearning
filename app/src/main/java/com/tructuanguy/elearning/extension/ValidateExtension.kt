package com.tructuanguy.elearning.extension

import com.tructuanguy.elearning.error.EmailError


fun isValidEmail(email: String): Boolean {
    val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+".toRegex()
    return email.matches(emailPattern)
}

fun isValidPasswordLength(password: String): Boolean {
    return password.length >= 6
}
