package com.tructuanguy.elearning.extension

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.ColorDrawable
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.common.SomethingWrongDialog
import com.tructuanguy.elearning.constants.PrefsConstants
import kotlinx.coroutines.Job

class SafeClickListener(
    private var defaultInterval: Long = 2000, private val onSafeCLick: (View) -> Unit
) : View.OnClickListener {
    private var lastTimeClicked: Long = 0
    override fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - lastTimeClicked < defaultInterval) {
            return
        }
        lastTimeClicked = SystemClock.elapsedRealtime()
        onSafeCLick(v)
    }
}

fun View.click(onSafeClick: (View) -> Unit) {
    val safeClickListener = SafeClickListener {
        onSafeClick(it)
    }
    setOnClickListener(safeClickListener)
}

fun View.gone() {
    visibility = View.GONE
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.showKeyboard(context: Context) {
    val imm: InputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun View.hideKeyboard(activity: Activity) {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.hideSoftInputFromWindow(activity.currentFocus?.windowToken, 0)
}

fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}


fun Context.showSimpleAlertDialog(
    title: String,
    message: String,
    negativeMsg: String,
    positiveMsg: String,
    onNegative: () -> Unit,
    onPositive: () -> Unit
) {
    val dialog = AlertDialog.Builder(this)
    dialog.setTitle(title)
    dialog.setMessage(message)
    dialog.setCancelable(false)
    dialog.setNegativeButton(negativeMsg) { di, _ ->
        onNegative.invoke()
        di.dismiss()
    }
    dialog.setPositiveButton(positiveMsg) { di, _ ->
        onPositive.invoke()
        di.dismiss()
    }
    val alert = dialog.create()
    alert.show()
    val negativeButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE)
    negativeButton.setTextColor(resources.getColor(R.color.black, null))
    val positiveButton = alert.getButton(DialogInterface.BUTTON_POSITIVE)
    positiveButton.setTextColor(resources.getColor(R.color.red_setting, null))
}

fun Fragment.showSomethingWrongDialog() {
    val dialog = SomethingWrongDialog()
    dialog.show(this.childFragmentManager, "progress_dialog")
}

fun Fragment.hideSomethingWrongDialog() {
    val dialog = this.childFragmentManager.findFragmentByTag("progress_dialog")
    if(dialog is SomethingWrongDialog) {
        dialog.dismiss()
    }
}