package com.tructuanguy.elearning.extension

import android.content.Intent
import android.provider.MediaStore
import android.webkit.WebView
import androidx.fragment.app.Fragment

fun Fragment.getLaunchGalleryIntent(): Intent {
    val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    return intent
}

fun Fragment.loadHtmlFromAsset(view: WebView, id: Int) {
    val detail = "file:///android_asset/detail/$id.html"
    view.loadUrl(detail)
}