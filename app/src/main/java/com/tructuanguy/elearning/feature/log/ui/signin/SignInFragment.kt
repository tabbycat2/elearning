package com.tructuanguy.elearning.feature.log.ui.signin

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.google.android.material.textfield.TextInputLayout
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.common.EditTextError
import com.tructuanguy.elearning.constants.FirebaseConstants.USER
import com.tructuanguy.elearning.databinding.FragmentSignInBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.log.ui.forgot_password.ForgotPasswordFragment
import com.tructuanguy.elearning.feature.log.ui.signup.SignUpFragment
import com.tructuanguy.elearning.feature.main.MainFragment
import com.tructuanguy.elearning.feature.main.ui.home.HomeFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignInFragment : BaseFragmentWithBinding<FragmentSignInBinding>() {

    private val signInViewModel: SignInViewModel by viewModels()

    override fun getViewBinding(inflater: LayoutInflater): FragmentSignInBinding {
        return FragmentSignInBinding.inflate(layoutInflater)
    }

    override fun init() {

    }

    override fun initData() {
        signInViewModel.signInState.observe(viewLifecycleOwner) {
            when(it) {
                is DataState.Success<Boolean> -> {
                    hideLoadingDialog()
                    signInViewModel.getUserData()
                }
                is DataState.Error -> {
                    hideLoadingDialog()
                    Log.i("signin_error", it.message)
                }
                is DataState.Loading -> {
                    showLoadingDialog()
                }
            }
        }
        signInViewModel.getUserDataState.observe(viewLifecycleOwner) {
            when(it) {
                is DataState.Success<User> -> {
                    hideLoadingDialog()
                    val bundle = Bundle()
                    bundle.putSerializable(USER, it.data)
                    openFragment(HomeFragment::class.java, bundle, false)
                }
                is DataState.Error -> {
                    hideLoadingDialog()
                }
                is DataState.Loading -> {
                    showLoadingDialog()
                }
            }
        }
    }

    override fun initAction() {
        binding.manageLocalUser.click {
            openFragment(ManageSavedAccountFragment::class.java, null, true)
        }
        binding.tvSignUpNow.click {
            openFragment(SignUpFragment::class.java, null, true)
        }

        binding.tvForgotPassword.click {
            openFragment(ForgotPasswordFragment::class.java, null, true)
        }

        binding.cvSignIn.click {
            if (signInViewModel.isOkForSignIn(
                    binding.edtEmail.text!!.trim().toString(),
                    binding.edtPassword.text!!.trim().toString()
                )
            ) {
                signInViewModel.signIn(
                    binding.edtEmail.text!!.trim().toString(),
                    binding.edtPassword.text!!.trim().toString(),
                )
            } else {
                signInViewModel.editTextError.observe(viewLifecycleOwner) {
                    when (it) {
                        EditTextError.EMAIL_EMPTY -> notifyError(binding.layoutEmail, getString(R.string.empty_email))
                        EditTextError.PASSWORD_EMPTY -> notifyError(binding.layoutPassword, getString(R.string.empty_password))
                        EditTextError.EMAIL_FORMAT -> notifyError(binding.layoutEmail, getString(R.string.email_format))
                        EditTextError.PASSWORD_LENGTH -> notifyError(binding.layoutPassword, getString(R.string.password_length))
                        else -> {}
                    }
                }
            }
        }
    }

    private fun notifyError(
        layoutEdt: TextInputLayout,
        error: String
    ) {
        layoutEdt.error = error
        layoutEdt.boxStrokeColor = ContextCompat.getColor(requireContext(), R.color.red)
    }

}