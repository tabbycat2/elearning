package com.tructuanguy.elearning.feature.main.ui.trivia.ui.play

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.databinding.DialogBotBinding
import com.tructuanguy.elearning.extension.click
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.random.Random

class BotSupportDialog : DialogFragment() {

    private lateinit var binding: DialogBotBinding
    private lateinit var listAnswer: ArrayList<String>
    private var rdA: Int = 0
    private var rdB: Int = 0
    private var rdC: Int = 0
    private var rdD: Int = 0
    private lateinit var answerA: String
    private lateinit var answerB: String
    private lateinit var answerC: String
    private lateinit var answerD: String
    private lateinit var trueAnswer: String
    private var rd1: Int = 0
    private var rd2: Int = 0
    private lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.full_screen_dialog)
        handler = Handler(Looper.myLooper()!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogBotBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listAnswer = arguments?.getStringArrayList("answer") as ArrayList<String>
        answerA = listAnswer[0]
        answerB = listAnswer[1]
        answerC = listAnswer[2]
        answerD = listAnswer[3]
        trueAnswer = listAnswer[4]
        Log.i("list", listAnswer.toString())

        rdA = Random.nextInt(0, 101)
        if (rdA == 100) {
            rdB = 0
            rdC = 0
            rdD = 0
        } else {
            rdB = Random.nextInt(0, 101 - rdA)
            if (rdB == 100) {
                rdA = 0
                rdC = 0
                rdD = 0
            } else {
                rdC = Random.nextInt(0, 101 - rdA - rdB)
                rdD = if (rdC == 0) {
                    0
                } else {
                    100 - rdA - rdB - rdC
                }
            }
        }

        createTwoRandom()

        val rdMax = maxOf(rdA, rdB, rdC, rdD)
        val rdMax2 = maxOf(rd1, rd2)

        binding.apply {
            cvOk.click { dismiss() }
            tvA.height = 1
            tvB.height = 1
            tvC.height = 1
            tvD.height = 1

            if(answerA != "" && answerB != "" && answerC != "" && answerD != ""){
                handler.postDelayed({
                    if (trueAnswer == answerA) {
                        if (rdA == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdA)
                            setCoroutine(tvB, tvPercentB, rdB)
                            setCoroutine(tvC, tvPercentC, rdC)
                            setCoroutine(tvD, tvPercentD, rdD)
                        } else if(rdB == rdMax){
                            setCoroutine(tvA, tvPercentA, rdB)
                            setCoroutine(tvB, tvPercentB, rdA)
                            setCoroutine(tvC, tvPercentC, rdC)
                            setCoroutine(tvD, tvPercentD, rdD)
                        } else if(rdC == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdC)
                            setCoroutine(tvB, tvPercentB, rdA)
                            setCoroutine(tvC, tvPercentC, rdB)
                            setCoroutine(tvD, tvPercentD, rdD)
                        } else if(rdD == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdD)
                            setCoroutine(tvB, tvPercentB, rdA)
                            setCoroutine(tvC, tvPercentC, rdB)
                            setCoroutine(tvD, tvPercentD, rdC)
                        }

                    } else if(trueAnswer == answerB){
                        if (rdA == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdB)
                            setCoroutine(tvB, tvPercentB, rdA)
                            setCoroutine(tvC, tvPercentC, rdC)
                            setCoroutine(tvD, tvPercentD, rdD)
                        } else if(rdB == rdMax){
                            setCoroutine(tvA, tvPercentA, rdA)
                            setCoroutine(tvB, tvPercentB, rdB)
                            setCoroutine(tvC, tvPercentC, rdC)
                            setCoroutine(tvD, tvPercentD, rdD)
                        } else if(rdC == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdA)
                            setCoroutine(tvB, tvPercentB, rdC)
                            setCoroutine(tvC, tvPercentC, rdB)
                            setCoroutine(tvD, tvPercentD, rdD)
                        } else if(rdD == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdA)
                            setCoroutine(tvB, tvPercentB, rdD)
                            setCoroutine(tvC, tvPercentC, rdB)
                            setCoroutine(tvD, tvPercentD, rdC)
                        }
                    } else if(trueAnswer == answerC) {
                        if (rdA == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdB)
                            setCoroutine(tvB, tvPercentB, rdC)
                            setCoroutine(tvC, tvPercentC, rdA)
                            setCoroutine(tvD, tvPercentD, rdD)
                        } else if (rdB == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdA)
                            setCoroutine(tvB, tvPercentB, rdC)
                            setCoroutine(tvC, tvPercentC, rdB)
                            setCoroutine(tvD, tvPercentD, rdD)
                        } else if (rdC == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdA)
                            setCoroutine(tvB, tvPercentB, rdB)
                            setCoroutine(tvC, tvPercentC, rdC)
                            setCoroutine(tvD, tvPercentD, rdD)
                        } else if (rdD == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdA)
                            setCoroutine(tvB, tvPercentB, rdB)
                            setCoroutine(tvC, tvPercentC, rdD)
                            setCoroutine(tvD, tvPercentD, rdC)
                        }
                    } else if(trueAnswer == answerD) {
                        if (rdA == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdB)
                            setCoroutine(tvB, tvPercentB, rdC)
                            setCoroutine(tvC, tvPercentC, rdD)
                            setCoroutine(tvD, tvPercentD, rdA)
                        } else if (rdB == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdA)
                            setCoroutine(tvB, tvPercentB, rdC)
                            setCoroutine(tvC, tvPercentC, rdD)
                            setCoroutine(tvD, tvPercentD, rdB)
                        } else if (rdC == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdA)
                            setCoroutine(tvB, tvPercentB, rdB)
                            setCoroutine(tvC, tvPercentC, rdD)
                            setCoroutine(tvD, tvPercentD, rdC)
                        } else if (rdD == rdMax) {
                            setCoroutine(tvA, tvPercentA, rdA)
                            setCoroutine(tvB, tvPercentB, rdB)
                            setCoroutine(tvC, tvPercentC, rdC)
                            setCoroutine(tvD, tvPercentD, rdD)
                        }
                    }
                },1500)
            } else {
                if(answerA == "" && answerB == ""){
                    if(trueAnswer == answerC){
                        if(rdMax2 == rd1){
                            setCoroutine(tvC, tvPercentC, rd1)
                            setCoroutine(tvD, tvPercentD, rd2)
                        } else if(rdMax2 == rd2){
                            setCoroutine(tvC, tvPercentC, rd2)
                            setCoroutine(tvD, tvPercentD, rd1)
                        }
                    } else if(trueAnswer == answerD) {
                        if (rdMax2 == rd1) {
                            setCoroutine(tvC, tvPercentC, rd2)
                            setCoroutine(tvD, tvPercentD, rd1)
                        } else if (rdMax2 == rd2) {
                            setCoroutine(tvC, tvPercentC, rd1)
                            setCoroutine(tvD, tvPercentD, rd2)
                        }
                    }
                } else if (answerA == "" && answerC == ""){
                    if(trueAnswer == answerB){
                        if(rdMax2 == rd1){
                            setCoroutine(tvB, tvPercentB, rd1)
                            setCoroutine(tvD, tvPercentD, rd2)
                        } else if(rdMax2 == rd2){
                            setCoroutine(tvB, tvPercentB, rd2)
                            setCoroutine(tvD, tvPercentD, rd1)
                        }
                    } else if(trueAnswer == answerD){
                        if(rdMax2 == rd1){
                            setCoroutine(tvB, tvPercentB, rd2)
                            setCoroutine(tvD, tvPercentD, rd1)
                        } else if(rdMax2 == rd2){
                            setCoroutine(tvB, tvPercentB, rd1)
                            setCoroutine(tvD, tvPercentD, rd2)
                        }
                    }
                } else if (answerA == "" && answerD == "") {
                    if (trueAnswer == answerB) {
                        if (rdMax2 == rd1) {
                            setCoroutine(tvB, tvPercentB, rd1)
                            setCoroutine(tvC, tvPercentC, rd2)
                        } else if (rdMax2 == rd2) {
                            setCoroutine(tvB, tvPercentB, rd2)
                            setCoroutine(tvC, tvPercentC, rd1)
                        }
                    } else if (trueAnswer == answerC) {
                        if (rdMax2 == rd1) {
                            setCoroutine(tvC, tvPercentC, rd1)
                            setCoroutine(tvB, tvPercentB, rd2)
                        } else if (rdMax2 == rd2) {
                            setCoroutine(tvC, tvPercentC, rd2)
                            setCoroutine(tvB, tvPercentB, rd1)
                        }
                    }
                } else if (answerB == "" && answerC == "") {
                    if (trueAnswer == answerA) {
                        if (rdMax2 == rd1) {
                            setCoroutine(tvA, tvPercentA, rd1)
                            setCoroutine(tvD, tvPercentD, rd2)
                        } else if (rdMax2 == rd2) {
                            setCoroutine(tvA, tvPercentA, rd2)
                            setCoroutine(tvD, tvPercentD, rd1)
                        }
                    } else if (trueAnswer == answerD) {
                        if (rdMax2 == rd1) {
                            setCoroutine(tvD, tvPercentD, rd1)
                            setCoroutine(tvA, tvPercentA, rd2)
                        } else if (rdMax2 == rd2) {
                            setCoroutine(tvD, tvPercentD, rd2)
                            setCoroutine(tvA, tvPercentA, rd1)
                        }
                    }
                } else if (answerB == "" && answerD == "") {
                    if (trueAnswer == answerA) {
                        if (rdMax2 == rd1) {
                            setCoroutine(tvA, tvPercentA, rd1)
                            setCoroutine(tvC, tvPercentC, rd2)
                        } else if (rdMax2 == rd2) {
                            setCoroutine(tvA, tvPercentA, rd2)
                            setCoroutine(tvC, tvPercentC, rd1)
                        }
                    } else if (trueAnswer == answerC) {
                        if (rdMax2 == rd1) {
                            setCoroutine(tvC, tvPercentC, rd1)
                            setCoroutine(tvA, tvPercentA, rd2)
                        } else if (rdMax2 == rd2) {
                            setCoroutine(tvC, tvPercentC, rd2)
                            setCoroutine(tvA, tvPercentA, rd1)
                        }
                    }
                } else if (answerC == "" && answerD == "") {
                    if (trueAnswer == answerA) {
                        if (rdMax2 == rd1) {
                            setCoroutine(tvA, tvPercentA, rd1)
                            setCoroutine(tvB, tvPercentB, rd2)
                        } else if (rdMax2 == rd2) {
                            setCoroutine(tvA, tvPercentA, rd2)
                            setCoroutine(tvB, tvPercentB, rd1)
                        }
                    } else if (trueAnswer == answerB) {
                        if (rdMax2 == rd1) {
                            setCoroutine(tvB, tvPercentB, rd1)
                            setCoroutine(tvA, tvPercentA, rd2)
                        } else if (rdMax2 == rd2) {
                            setCoroutine(tvB, tvPercentB, rd2)
                            setCoroutine(tvA, tvPercentA, rd1)
                        }
                    }
                }
            }

        }


    }

    @SuppressLint("SetTextI18n")
    private fun setCoroutine(tv1: TextView, tv2: TextView, rd: Int) {
        lifecycleScope.launch(Dispatchers.Main) {
            for (i in 0..rd) {
                tv1.height = i * 8
                tv2.text = "$i%"
                delay(1)
            }
        }
    }


    private fun createTwoRandom() {
        rd1 = Random.nextInt(1, 101)
        rd2 = 100 - rd1
    }
}