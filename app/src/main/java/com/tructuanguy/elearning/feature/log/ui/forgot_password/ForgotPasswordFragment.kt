package com.tructuanguy.elearning.feature.log.ui.forgot_password

import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.databinding.FragmentForgottenPasswordBinding
import com.tructuanguy.elearning.error.EmailError
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.extension.visible
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotPasswordFragment : BaseFragmentWithBinding<FragmentForgottenPasswordBinding>() {

    private val passwordViewModel: ForgotPasswordViewModel by viewModels()

    override fun getViewBinding(inflater: LayoutInflater): FragmentForgottenPasswordBinding {
        return FragmentForgottenPasswordBinding.inflate(layoutInflater)
    }

    override fun init() {
        binding.cvSubmitEmail.click {
            passwordViewModel.forgotPassword(binding.edtEmail.text.toString().trim())
        }
    }

    override fun initData() {
        passwordViewModel.forgotPasswordState.observe(viewLifecycleOwner) {
            when(it) {
                is DataState.Loading -> {
                    passwordViewModel.onSubmitEmail(binding.edtEmail.text.toString().trim())
                    showLoadingDialog()
                }
                is DataState.Success -> {
                    binding.layoutSendEmail.visible()
                    hideLoadingDialog()
                }
                is DataState.Error -> {
                    hideLoadingDialog()
                }
            }
        }
        passwordViewModel.errorEmailState.observe(viewLifecycleOwner) {
            when(it) {
                EmailError.EMPTY -> showEmailError(getString(R.string.empty_email))
                EmailError.NO_INVALID -> showEmailError(getString(R.string.email_format))
                else -> {}
            }
        }
    }


    override fun initAction() {
        binding.icBack.click {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
    }

    private fun showEmailError(error: String) {
        binding.tvError.visible()
        binding.tvError.text = error
    }
}