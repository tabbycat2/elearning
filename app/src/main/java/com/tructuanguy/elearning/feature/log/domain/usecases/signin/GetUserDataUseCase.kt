package com.tructuanguy.elearning.feature.log.domain.usecases.signin

import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.log.domain.repository.AuthenticationRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetUserDataUseCase @Inject constructor(
    private val authenticationRepository: AuthenticationRepository
) {
    suspend operator fun invoke(): Flow<DataState<User>> = authenticationRepository.getUserData()
}