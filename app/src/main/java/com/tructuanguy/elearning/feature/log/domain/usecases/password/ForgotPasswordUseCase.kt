package com.tructuanguy.elearning.feature.log.domain.usecases.password

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.log.domain.repository.AuthenticationRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ForgotPasswordUseCase @Inject constructor(
    private val authenticationRepository: AuthenticationRepository
) {
    suspend operator fun invoke(email: String): Flow<DataState<Boolean>> = authenticationRepository.forgotPassword(email)
}