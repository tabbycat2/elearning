package com.tructuanguy.elearning.feature.main.ui.home.domain.repository

import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.common.DataState
import kotlinx.coroutines.flow.Flow

interface MainHomeRepository {

    fun getAllUser(): Flow<DataState<List<User>>>

}