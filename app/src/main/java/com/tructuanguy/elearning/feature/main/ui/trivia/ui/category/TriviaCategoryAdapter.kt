package com.tructuanguy.elearning.feature.main.ui.trivia.ui.category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.elearning.databinding.ItemTriviaBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.model.TriviaCategory

class TriviaCategoryAdapter(
    private val list: List<TriviaCategory>,
    private val listener: (TriviaCategory) -> Unit
) : RecyclerView.Adapter<TriviaCategoryAdapter.TriviaCategoryHolder>() {

    inner class TriviaCategoryHolder(private val binding: ItemTriviaBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TriviaCategory) {
            binding.tvCategoryName.text = item.name
            binding.ivCategory.setImageResource(item.image)
            binding.tvCategorySize.text = item.size.toString()
            itemView.click {
                listener.invoke(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TriviaCategoryHolder {
        return TriviaCategoryHolder(ItemTriviaBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TriviaCategoryHolder, position: Int) {
        holder.bind(list[position])
    }

}