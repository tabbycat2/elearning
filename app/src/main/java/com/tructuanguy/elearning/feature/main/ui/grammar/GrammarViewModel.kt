package com.tructuanguy.elearning.feature.main.ui.grammar

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.usecases.grammar.GetChildGrammar
import com.tructuanguy.elearning.db.domain.usecases.grammar.GetParentGrammar
import com.tructuanguy.elearning.db.entities.Grammar
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GrammarViewModel @Inject constructor(
    private val getParentGrammarUseCase: GetParentGrammar,
    private val getChildGrammarUseCase: GetChildGrammar
) : ViewModel() {

    private val _getParentGrammarState = MutableLiveData<DataState<List<Grammar>>>()
    val getParentGrammarState: LiveData<DataState<List<Grammar>>>
        get() = _getParentGrammarState

    private val _getChildGrammarState = MutableLiveData<DataState<List<Grammar>>>()
    val getChildGrammarState: LiveData<DataState<List<Grammar>>>
        get() = _getChildGrammarState

    fun getParentGrammar() {
        viewModelScope.launch(Dispatchers.IO) {
            getParentGrammarUseCase().onEach {
                _getParentGrammarState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun getChildGrammar() {
        viewModelScope.launch(Dispatchers.IO) {
            getChildGrammarUseCase().onEach {
                _getChildGrammarState.value = it
            }.launchIn(viewModelScope)
        }
    }

}