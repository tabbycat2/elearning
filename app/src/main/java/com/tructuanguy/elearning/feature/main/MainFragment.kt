package com.tructuanguy.elearning.feature.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.constants.FirebaseConstants.USER
import com.tructuanguy.elearning.databinding.FragmentMainBinding
import com.tructuanguy.elearning.feature.main.ui.course.CourseFragment
import com.tructuanguy.elearning.feature.main.ui.home.HomeFragment
import com.tructuanguy.elearning.feature.main.ui.me.MeFragment

class MainFragment : BaseFragmentWithBinding<FragmentMainBinding>() {

    private lateinit var user: User

    override fun getViewBinding(inflater: LayoutInflater): FragmentMainBinding {
        return FragmentMainBinding.inflate(layoutInflater)
    }

    override fun init() {
        user = requireArguments().getSerializable(USER) as User
        Log.i("user", user.toString())
    }

    override fun initData() {

    }

    override fun initAction() {
//        binding.bottomNav.setOnItemSelectedListener {
//            when(it.itemId) {
//                R.id.home -> openFragment(HomeFragment::class.java, null, false, R.id.fr_main)
//                R.id.course -> openFragment(CourseFragment::class.java, null, false, R.id.fr_main)
//                R.id.me -> openFragment(MeFragment::class.java, createUserBundle(), false, R.id.fr_main)
//            }
//            true
//        }
//        binding.bottomNav.selectedItemId = R.id.home
    }

    private fun createUserBundle(): Bundle {
        val bundle = Bundle()
        bundle.putSerializable(USER, user)
        return bundle
    }
}