package com.tructuanguy.elearning.feature.log.domain.usecases.signup

import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.feature.log.domain.repository.AuthenticationRepository
import javax.inject.Inject

class SaveUserFireStoreUseCase @Inject constructor(
    private val authenticationRepository: AuthenticationRepository
) {
    suspend operator fun invoke(user: User) =
        authenticationRepository.saveUserFireStore(user)
}