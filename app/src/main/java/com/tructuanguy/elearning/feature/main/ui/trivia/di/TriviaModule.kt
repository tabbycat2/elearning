package com.tructuanguy.elearning.feature.main.ui.trivia.di

import com.google.firebase.firestore.CollectionReference
import com.tructuanguy.elearning.constants.ApiConstants
import com.tructuanguy.elearning.di.FirebaseModule
import com.tructuanguy.elearning.feature.main.ui.trivia.domain.data.TriviaRepositoryImpl
import com.tructuanguy.elearning.feature.main.ui.trivia.domain.repository.TriviaRepository
import com.tructuanguy.elearning.feature.main.ui.trivia.model.api.TriviaApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object TriviaModule {

    @Provides
    @Singleton
    fun provideQuizRepository(api: TriviaApi, @FirebaseModule.UserCollection userCollection: CollectionReference) : TriviaRepository{
        return TriviaRepositoryImpl(api, userCollection)
    }

    @Provides
    @Singleton
    fun provideQuizApi(okHttpClient: OkHttpClient): TriviaApi {
        return Retrofit.Builder()
            .baseUrl(ApiConstants.TRIVIA_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
            .create(TriviaApi::class.java)
    }

}