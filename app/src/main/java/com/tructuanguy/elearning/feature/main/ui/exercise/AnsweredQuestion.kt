package com.tructuanguy.elearning.feature.main.ui.exercise

import com.tructuanguy.elearning.db.entities.ExamQuestion

data class AnsweredQuestion(
    val chosenAnswer: Int,
    val exe: ExamQuestion
)
