package com.tructuanguy.elearning.feature.main.ui.quote

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.elearning.databinding.ItemKeyWordQuoteBinding
import com.tructuanguy.elearning.db.entities.KeyWordQuote

class KeyWordQuoteAdapter(
    private val list: List<KeyWordQuote>
) : RecyclerView.Adapter<KeyWordQuoteAdapter.KeyWordQuoteHolder>() {

    inner class KeyWordQuoteHolder(private val binding: ItemKeyWordQuoteBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(word: KeyWordQuote) {
            binding.tvOriginal.text = "${word.original} - "
            binding.tvPronunciation.text = "${word.pronunciation}:"
            binding.tvType.text = " (${word.type}) "
            binding.tvMean.text = word.mean
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KeyWordQuoteHolder {
        return KeyWordQuoteHolder(ItemKeyWordQuoteBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: KeyWordQuoteHolder, position: Int) {
        holder.bind(list[position])
    }

}