package com.tructuanguy.elearning.feature.main.ui.story

import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.databinding.FragmentStoryBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.ui.story.adapter.LessonAdapter
import dagger.hilt.android.AndroidEntryPoint

const val LESSON = "lesson"

@AndroidEntryPoint
class StoryFragment : BaseFragmentWithBinding<FragmentStoryBinding>() {

    private val storyViewModel: StoryViewModel by viewModels()

    override fun getViewBinding(inflater: LayoutInflater): FragmentStoryBinding {
        return FragmentStoryBinding.inflate(layoutInflater)
    }

    override fun init() {
        storyViewModel.getListLesson()
    }

    override fun initData() {
        storyViewModel.listLesson.observe(viewLifecycleOwner) {
            binding.rcvLesson.adapter = LessonAdapter(it) { lesson ->
                openFragment(
                    LessonFragment::class.java,
                    createBundle(LESSON, lesson),
                    true,
                    R.id.fr_app
                )
            }
        }
    }

    override fun initAction() {
        binding.icBack.click { onBackPressed() }
    }


}