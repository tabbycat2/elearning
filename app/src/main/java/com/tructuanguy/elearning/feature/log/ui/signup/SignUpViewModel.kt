package com.tructuanguy.elearning.feature.log.ui.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.common.EditTextError
import com.tructuanguy.elearning.extension.isValidEmail
import com.tructuanguy.elearning.extension.isValidPasswordLength
import com.tructuanguy.elearning.feature.log.domain.usecases.signup.SaveUserFireStoreUseCase
import com.tructuanguy.elearning.feature.log.domain.usecases.signup.SignUpUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val signUpUseCase: SignUpUseCase,
    private val saveUserFireStoreUseCase: SaveUserFireStoreUseCase
) : ViewModel() {

    private val _editTextError = MutableLiveData<EditTextError>()
    val editTextError: LiveData<EditTextError>
        get() = _editTextError

    private val _signUpState: MutableLiveData<DataState<User>> = MutableLiveData()
    val signUpState: LiveData<DataState<User>>
        get() = _signUpState

    private val _saveUserFireStoreState: MutableLiveData<DataState<Boolean>> = MutableLiveData()
    val saveUserFireStoreState: LiveData<DataState<Boolean>>
        get() = _saveUserFireStoreState

    fun signUp(email: String, password: String, username: String) {
        viewModelScope.launch(Dispatchers.Main) {
            signUpUseCase(email, password, username).onEach {
                _signUpState.value = it
            }.launchIn(viewModelScope)
        }
    }


    fun saveUserFireStore(user: User) {
        viewModelScope.launch(Dispatchers.Main) {
            saveUserFireStoreUseCase(user).onEach {
                _saveUserFireStoreState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun isOkSignUp(email: String, password: String, username: String): Boolean {
        if (email.trim().isEmpty()) {
            _editTextError.value = EditTextError.EMAIL_EMPTY
            return false
        } else if (email.trim().isNotEmpty() && !isValidEmail(email)) {
            _editTextError.value = EditTextError.EMAIL_FORMAT
            return false
        } else if (password.isNotEmpty() && !isValidPasswordLength(password)) {
            _editTextError.value = EditTextError.PASSWORD_LENGTH
            return false
        } else if (password.trim().isEmpty()) {
            _editTextError.value = EditTextError.PASSWORD_EMPTY
            return false
        } else if (username.trim().isEmpty()) {
            _editTextError.value = EditTextError.USERNAME_EMPTY
            return false
        }
        return true
    }

}