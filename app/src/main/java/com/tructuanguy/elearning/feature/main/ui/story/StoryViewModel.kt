package com.tructuanguy.elearning.feature.main.ui.story

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.usecases.story.GetStory
import com.tructuanguy.elearning.db.domain.usecases.story.GetStoryNumber
import com.tructuanguy.elearning.db.entities.Lesson
import com.tructuanguy.elearning.db.entities.Story
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StoryViewModel @Inject constructor(
    private val getStoryUseCase: GetStory,
    private val getStoryNumberUseCase: GetStoryNumber
) : ViewModel() {

    val listLesson = MutableLiveData<ArrayList<Lesson>>()
    private val mListLesson = ArrayList<Lesson>()

    private val _getStoryState = MutableLiveData<DataState<List<Story>>>()
    val getStoryState: LiveData<DataState<List<Story>>>
        get() = _getStoryState

    private val _getStoryNumberState = MutableLiveData<DataState<List<Int>>>()
    val getStoryNumberState: LiveData<DataState<List<Int>>>
        get() = _getStoryNumberState

    fun getStory(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            getStoryUseCase(id).onEach {
                _getStoryState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun getStoryNumber() {
        viewModelScope.launch(Dispatchers.IO) {
            getStoryNumberUseCase().onEach {
                _getStoryNumberState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun getListLesson(): ArrayList<Lesson> {
        mListLesson.add(Lesson(0,"Truyện ngắn"))
        mListLesson.add(Lesson(1,"Truyện cổ tích"))
        mListLesson.add(Lesson(2,"Truyện cười"))
        mListLesson.add(Lesson(3,"Truyện ngụ ngôn"))
        mListLesson.add(Lesson(4,"Bài luận mẫu"))
        mListLesson.add(Lesson(5,"Báo song ngữ"))
        mListLesson.add(Lesson(6,"Châm ngôn hay"))
        mListLesson.add(Lesson(7,"Mẫu câu giao tiếp"))
        mListLesson.add(Lesson(8,"Cambridge IELTS"))
        listLesson.value = mListLesson
        return mListLesson
    }

}