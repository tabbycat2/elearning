package com.tructuanguy.elearning.feature.main.ui.exercise

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.Button
import android.widget.GridView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.databinding.FragmentDetailExerciseBinding
import com.tructuanguy.elearning.db.entities.ExamQuestion
import com.tructuanguy.elearning.extension.click
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailExerciseFragment : BaseFragmentWithBinding<FragmentDetailExerciseBinding>() {

    companion object {
        var checkResult = false
    }

    private val exeViewModel: ExerciseViewModel by viewModels()
    private var sumCorrect = 0
    private var sumUnAnswered = 0
    private var scoreInScaleTen = 0.00
    private var isDialogNotification = false
    private var isDialogConfirm = false
    private var isDialogTimeUp = false
    private var isDialogListQuestion = false
    private var dialog: Dialog? = null

    override fun getViewBinding(inflater: LayoutInflater): FragmentDetailExerciseBinding {
        return FragmentDetailExerciseBinding.inflate(layoutInflater)
    }

    override fun init() {
        createDialog()
        exeViewModel.getExamQuestion(1, 12)
    }


    private fun createDialog() {
        dialog = Dialog(requireContext())
        if (dialog?.window != null) {
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.setCancelable(false)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun initData() {

        exeViewModel.getExamQuestionState.observe(viewLifecycleOwner) { state ->
            when (state) {
                is DataState.Error -> {
                    Log.i("error", state.message)
                }

                is DataState.Loading -> {
                }

                is DataState.Success -> {
                    Log.i("ddd", state.data.toString())
                    val list = state.data
                    binding.vpgQuestion.adapter = DetailExeAdapter(list, requireContext())
                    binding.vpgQuestion.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                        override fun onPageScrolled(
                            position: Int,
                            positionOffset: Float,
                            positionOffsetPixels: Int
                        ) {
                            binding.tvCurrent.text = "Question ${position + 1}/50"
                        }

                        override fun onPageSelected(position: Int) {

                        }

                        override fun onPageScrollStateChanged(state: Int) {

                        }
                    })
                    binding.submit.click {
                        if (!checkResult) showDialogNotification(list) else endTest()
                    }
                    if (isDialogNotification) showDialogNotification(list)
                    else if (isDialogConfirm) showDialogConfirm(list)
                    else if (isDialogListQuestion) showDialogListQuestion(list)
                    else if (isDialogTimeUp) showDialogTimeUp()
                    else dialog?.dismiss()
                }
            }
        }
    }

    private fun showDialogTimeUp() {
        removeCheckDialog()
    }


    @SuppressLint("CutPasteId")
    private fun showDialogListQuestion(list: List<ExamQuestion>) {
        removeCheckDialog()
        dialog?.setContentView(R.layout.dialog_list_question)
        dialog?.findViewById<RelativeLayout>(R.id.layoutQuestions)?.layoutParams?.width = requireActivity().window.decorView.width
        dialog?.findViewById<Button>(R.id.btnDlListQ_EndQuiz)?.setOnClickListener {
            dialog?.dismiss()
            removeCheckDialog()
            showDialogNotification(list)
        }
        dialog?.findViewById<Button>(R.id.btnDlListQ_Return)?.setOnClickListener {
            dialog?.dismiss()
            removeCheckDialog()
        }
        val listQuestionAdapter = ListQuestionAdapter(requireContext(), list)
        dialog?.findViewById<GridView>(R.id.dlListQ_GridItem)?.adapter = listQuestionAdapter
        if (!checkResult) {
            dialog?.findViewById<TextView>(R.id.dlListQ_Title)?.setText(R.string.title_quiz_ques)
        } else {
            dialog?.findViewById<TextView>(R.id.dlListQ_Title)?.setText(R.string.title_quiz_result)
            dialog?.findViewById<Button>(R.id.btnDlListQ_EndQuiz)?.visibility = View.GONE
            dialog?.findViewById<LinearLayout>(R.id.layoutResult)?.visibility = View.VISIBLE
            dialog?.findViewById<TextView>(R.id.resultTrue)?.text = sumCorrect.toString()
            dialog?.findViewById<TextView>(R.id.resultFalse)?.text =
                (list.size - sumUnAnswered - sumCorrect).toString()
            dialog?.findViewById<TextView>(R.id.resultUnChoice)?.text = sumUnAnswered.toString()
            dialog?.findViewById<TextView>(R.id.resultScore)?.text = scoreInScaleTen.toString()
        }
        dialog?.show()
        isDialogListQuestion = true
        dialog?.findViewById<GridView>(R.id.dlListQ_GridItem)?.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->
                dialog?.dismiss()
                removeCheckDialog()
                binding.vpgQuestion.currentItem = position
            }
    }

    private fun endTest() {
        removeCheckDialog()
    }

    private fun removeCheckDialog() {
        isDialogNotification = false
        isDialogConfirm = false
        isDialogListQuestion = false
        isDialogTimeUp = false
    }

    private fun showDialogNotification(list: List<ExamQuestion>) {
        removeCheckDialog()
        dialog?.setContentView(R.layout.dialog_notification)
        dialog?.findViewById<Button>(R.id.btnDlNotifyReturn)?.click {
            dialog?.dismiss()
            removeCheckDialog()
        }
        dialog?.findViewById<Button>(R.id.btnDlNotifyEnd)?.click {
            dialog?.dismiss()
            removeCheckDialog()
            showDialogConfirm(list)
        }
        var unAns = 0
        for (i in list.indices) {
            if (list[i].t_answer == 0) unAns++
        }
        if (unAns > 0) {
            dialog?.findViewById<TextView>(R.id.tvDlNotifyMessage)?.text =
                String.format(
                    "Vẫn còn %d/%d câu chưa được trả lời. Bạn vẫn muốn nộp?".trimIndent(),
                    unAns,
                    list.size
                )
            dialog?.show()
            isDialogNotification = true
        } else showDialogConfirm(list)
    }

    private fun showDialogConfirm(list: List<ExamQuestion>) {
        removeCheckDialog()
        dialog?.setContentView(R.layout.dialog_confirm)
        dialog?.findViewById<Button>(R.id.btnDlConfirmOK)?.setOnClickListener {
            dialog?.dismiss()
            removeCheckDialog()
            showResultUI(list)
        }
        dialog?.findViewById<Button>(R.id.btnDlConfirmCancel)?.setOnClickListener {
            dialog?.dismiss()
            removeCheckDialog()
        }
        dialog?.show()
        isDialogConfirm = true
    }

    private fun showResultUI(list: List<ExamQuestion>) {
        checkResult = true
        parseResult(list)
        showDialogListQuestion(list)
        binding.vpgQuestion.adapter = DetailExeAdapter(list, requireContext())
        if(checkResult) binding.submit.text = getString(R.string.close)
    }

    private fun parseResult(list: List<ExamQuestion>) {
        for (i in list.indices) {
            val choice = list[i].t_answer
            if (choice == 0) sumUnAnswered++
            if (list[i].t_answer == list[i].t_key) sumCorrect++
        }
        scoreInScaleTen = (sumCorrect.toDouble() / list.size.toDouble()) * 10
    }


    override fun initAction() {
        binding.icBack.click {
            onBackPressed()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        checkResult = false
    }

}