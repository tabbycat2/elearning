package com.tructuanguy.elearning.feature.main.ui.quiz

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.usecases.word.GetAllWord
import com.tructuanguy.elearning.db.entities.Word
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QuizViewModel @Inject constructor(
    private val getAllWordUseCase: GetAllWord
) : ViewModel() {

    private val _getAllWordState = MutableLiveData<DataState<List<Word>>>()
    val getAllWordState: LiveData<DataState<List<Word>>>
        get() = _getAllWordState

    fun getAllWord() {
        viewModelScope.launch(Dispatchers.IO) {
            getAllWordUseCase().onEach {
                _getAllWordState.value = it
            }.launchIn(viewModelScope)
        }
    }

}