package com.tructuanguy.elearning.feature.main.ui.trivia.domain.usecase

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.main.ui.trivia.domain.repository.TriviaRepository
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.model.TriviaItem
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetTriviaCategory @Inject constructor(
    private val triviaRepository: TriviaRepository
) {
    suspend operator fun invoke(category: String): Flow<DataState<List<TriviaItem>>> = triviaRepository.getCategoryQuiz(category)
}