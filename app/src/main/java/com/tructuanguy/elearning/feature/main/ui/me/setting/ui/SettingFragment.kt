package com.tructuanguy.elearning.feature.main.ui.me.setting.ui

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.pm.PackageManager
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.constants.AppConstants
import com.tructuanguy.elearning.constants.FirebaseConstants.INFO_NOT_SET
import com.tructuanguy.elearning.constants.FirebaseConstants.USER
import com.tructuanguy.elearning.constants.PrefsConstants
import com.tructuanguy.elearning.databinding.FragmentSettingBinding
import com.tructuanguy.elearning.db.entities.UserLocal
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.extension.getLaunchGalleryIntent
import com.tructuanguy.elearning.extension.showSimpleAlertDialog
import com.tructuanguy.elearning.feature.log.ui.signin.SignInFragment
import com.tructuanguy.elearning.feature.log.ui.signin.SignInViewModel
import com.tructuanguy.elearning.feature.main.ui.me.setting.ui.adapter.SettingAdapter
import com.tructuanguy.elearning.feature.main.ui.me.setting.ui.change_password.ChangePasswordFragment
import com.tructuanguy.elearning.feature.main.ui.me.setting.ui.model.SettingItem
import com.tructuanguy.elearning.helper.PermissionHelper
import com.tructuanguy.elearning.helper.PreferenceHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SettingFragment : BaseFragmentWithBinding<FragmentSettingBinding>() {

    @Inject
    lateinit var mPrefs: PreferenceHelper

    @Inject
    lateinit var mPermissionHelper: PermissionHelper

    private val settingViewModel: SettingViewModel by viewModels()
    private val signInViewModel: SignInViewModel by viewModels()
    private lateinit var user: User

    override fun getViewBinding(inflater: LayoutInflater): FragmentSettingBinding {
        return FragmentSettingBinding.inflate(layoutInflater)
    }

    override fun init() {


        user = arguments?.getSerializable(USER) as User
        if(user.image.isNotEmpty()) {
            Glide.with(requireContext()).load(user.image).into(binding.avatar)
        } else {
            binding.avatar.setImageResource(R.drawable.default_img_user)
        }

        settingViewModel.getSettingFeature()
        signInViewModel.getUserData()

        binding.apply {
            icBack.click { onBackPressed() }
            ivEditUserImage.click {
                if (mPermissionHelper.isStoragePermissionGranted()) {
                    pickImage.launch(getLaunchGalleryIntent())
                } else {
                    requestPermissions(
                        arrayOf(
                            if (mPermissionHelper.isAndroid13()) Manifest.permission.READ_MEDIA_IMAGES else Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), AppConstants.REQUEST_PERMISSION_CODE
                    )
                }
            }
        }

    }

    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == AppConstants.REQUEST_PERMISSION_CODE && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage.launch(getLaunchGalleryIntent())
        } else {
            toast("denied")
        }
    }

    override fun initData() {
        settingViewModel.listSettingItem.observe(viewLifecycleOwner) {
            val handleSettingItemClick: (SettingItem, Int) -> Unit = { _, pos ->
                when (pos) {
                    0 -> { // change password
                        openFragment(
                            ChangePasswordFragment::class.java,
                            createBundle(USER, user),
                            true,
                            R.id.fr_app
                        )
                    }

                    5 -> { // log out
                        setUpLogOutDialog()
                    }
                }
            }
            binding.rcvSetting.adapter = SettingAdapter(it, handleSettingItemClick)
        }

        settingViewModel.updateUserImageState.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Error -> {
                    hideLoadingDialog()
                }

                is DataState.Loading -> {
                    showLoadingDialog()
                }

                is DataState.Success -> {
                    hideLoadingDialog()
                    signInViewModel.getUserData()
                }
            }
        }

        signInViewModel.logOutState.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Loading -> {
                    showLoadingDialog()
                }

                is DataState.Success -> {
                    hideLoadingDialog()
                    navigateSignInScreen()
                }

                is DataState.Error -> {
                    hideLoadingDialog()
                }
            }
        }

        settingViewModel.isSavedLocalUserState.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Error -> {
                }

                is DataState.Loading -> {
                }

                is DataState.Success -> {
                    val isSaved = it.data
                    if (!isSaved) {
                        settingViewModel.insertUserLocal(
                            UserLocal(
                                id = 0,
                                email = user.email,
                                password = user.password,
                                name = user.userName
                            )
                        )
                    } else {
                        navigateSignInScreen()
                    }
                }
            }
        }

        settingViewModel.insertUserLocalState.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Error -> {
                    hideLoadingDialog()
                }

                is DataState.Loading -> {
                    showLoadingDialog()
                }

                is DataState.Success -> {
                    hideLoadingDialog()
                    signInViewModel.logOut()
                }
            }
        }
    }

    override fun initAction() {

    }

    private val pickImage = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == RESULT_OK) {
            val uri = result.data!!.data
            settingViewModel.updateUserImage(uri!!)
        }
    }

    private fun setUpLogOutDialog() {
        requireContext().showSimpleAlertDialog(
            title = getString(R.string.log_out),
            message = getString(R.string.ask_save_account),
            negativeMsg = getString(R.string.cancel),
            positiveMsg = getString(R.string.ok),
            onNegative = {
                mPrefs.apply {
                    saveString(PrefsConstants.USER_EMAIL, "")
                    saveString(PrefsConstants.USER_PASSWORD, "")
                }
                navigateSignInScreen()
            },
            onPositive = {
                mPrefs.apply {
                    saveString(PrefsConstants.USER_EMAIL, user.email)
                    saveString(PrefsConstants.USER_PASSWORD, user.password)
                }
                Handler(Looper.getMainLooper()).postDelayed({
                    settingViewModel.checkSavedUserLocal(user.email)
                }, 250)
            }
        )
    }

    private fun navigateSignInScreen() {
        openFragment(SignInFragment::class.java, null, false, R.id.fr_app)
        val fm = activity?.supportFragmentManager
        for(i in 0 until fm!!.backStackEntryCount) {
            fm.popBackStack()
        }
    }

}