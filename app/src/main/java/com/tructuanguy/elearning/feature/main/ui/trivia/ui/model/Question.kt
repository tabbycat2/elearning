package com.tructuanguy.elearning.feature.main.ui.trivia.ui.model

data class Question(
    val text: String
)