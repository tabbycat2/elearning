package com.tructuanguy.elearning.feature.main.ui.vocabulary

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tructuanguy.elearning.databinding.ItemVocabularyVerticalBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.model.Topic

class VocabularyAdapter(
    private val list: List<Topic>,
    private val listener:(Topic, Int) -> Unit
) : RecyclerView.Adapter<VocabularyAdapter.VocabularyHolder>() {

    inner class VocabularyHolder(private val binding: ItemVocabularyVerticalBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(topic: Topic) {
            Glide.with(itemView.context).load(topic.image).into(binding.ivTopic)
            binding.tvTopicName.text = topic.topicName
            itemView.click {
                listener.invoke(topic, adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): VocabularyAdapter.VocabularyHolder {
        return VocabularyHolder(ItemVocabularyVerticalBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: VocabularyAdapter.VocabularyHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }


}