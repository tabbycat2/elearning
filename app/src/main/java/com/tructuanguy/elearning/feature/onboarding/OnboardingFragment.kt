package com.tructuanguy.elearning.feature.onboarding

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayoutMediator
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.databinding.FragmentOnboardingBinding
import com.tructuanguy.elearning.feature.log.ui.signin.SignInFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingFragment : BaseFragmentWithBinding<FragmentOnboardingBinding>() {

    private lateinit var viewModel: OnBoardingViewModel
    override fun getViewBinding(inflater: LayoutInflater): FragmentOnboardingBinding {
        return FragmentOnboardingBinding.inflate(layoutInflater)
    }

    override fun init() {

    }

    override fun initData() {
        viewModel = ViewModelProvider(this@OnboardingFragment)[OnBoardingViewModel::class.java]
        viewModel.apply {
            getOnBoardingImages()
            listOnboardingImages.observe(this@OnboardingFragment) {
                binding.apply {
                    viewpagerOnboarding.adapter = OnBoardingAdapter(it) {
                        showLoadingDialog()
                        Handler(Looper.getMainLooper()).postDelayed({
                            hideLoadingDialog()
                            openFragment(SignInFragment::class.java, null, false)
                        }, 1500)
                    }
                    TabLayoutMediator(tabDots, viewpagerOnboarding) { _, _ -> }.attach()
                }
            }
        }
    }

    override fun initAction() {

    }


}