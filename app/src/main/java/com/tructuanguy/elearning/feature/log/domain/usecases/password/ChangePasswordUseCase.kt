package com.tructuanguy.elearning.feature.log.domain.usecases.password

import com.tructuanguy.elearning.feature.log.domain.repository.AuthenticationRepository
import javax.inject.Inject

class ChangePasswordUseCase @Inject constructor(
    private val authenticationRepository: AuthenticationRepository
) {
    suspend operator fun invoke(email: String, oldPassword: String, newPassword: String) =
        authenticationRepository.changePassword(email, oldPassword, newPassword)
}