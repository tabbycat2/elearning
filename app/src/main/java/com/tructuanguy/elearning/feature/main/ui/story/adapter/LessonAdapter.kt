package com.tructuanguy.elearning.feature.main.ui.story.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.elearning.databinding.ItemLessonBinding
import com.tructuanguy.elearning.db.entities.Lesson
import com.tructuanguy.elearning.extension.click

class LessonAdapter(
    private val list: ArrayList<Lesson>,
    private val listener: (Lesson) -> Unit
) : RecyclerView.Adapter<LessonAdapter.LessonHolder>() {

    inner class LessonHolder(private val binding: ItemLessonBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(lesson: Lesson) {
            binding.tvName.text = lesson.name
            itemView.click {
                listener.invoke(lesson)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LessonHolder {
        return LessonHolder(ItemLessonBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: LessonHolder, position: Int) {
        holder.bind(list[position])
    }


}