package com.tructuanguy.elearning.feature.main.ui.vocabulary

import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.databinding.FragmentVocabularyBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.model.Topic
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VocabularyFragment : BaseFragmentWithBinding<FragmentVocabularyBinding>() {

    private val vocabularyViewModel: VocabularyViewModel by viewModels()

    override fun getViewBinding(inflater: LayoutInflater): FragmentVocabularyBinding {
        return FragmentVocabularyBinding.inflate(layoutInflater)
    }

    override fun init() {
        vocabularyViewModel.getAllTopic()
    }

    override fun initData() {
        vocabularyViewModel.getAllTopicState.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Error -> {
                    hideLoadingDialog()
                    Log.i("error", it.message)
                }

                is DataState.Loading -> {
                    showLoadingDialog()
                }

                is DataState.Success -> {
                    hideLoadingDialog()
                    binding.rcvVocabulary.adapter =
                        VocabularyAdapter(it.data) { topic, pos ->
                            openFragment(DetailVocabularyFragment::class.java, createBundle("topic", topic), true, R.id.fr_app)
                        }
                }
            }
        }
    }

    override fun initAction() {
        binding.icBack.click { onBackPressed() }
    }


}