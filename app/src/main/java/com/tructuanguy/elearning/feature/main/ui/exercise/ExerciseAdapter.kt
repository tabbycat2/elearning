package com.tructuanguy.elearning.feature.main.ui.exercise

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.elearning.databinding.ItemExerciseGeneralBinding
import com.tructuanguy.elearning.extension.click

class ExerciseAdapter(
    private val listener: (Int) -> Unit
) : RecyclerView.Adapter<ExerciseAdapter.ExerciseHolder>() {

    inner class ExerciseHolder(private val binding: ItemExerciseGeneralBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind() {
            binding.tvExerciseGeneral.text = "Bộ đề số ${adapterPosition+ 1}"
            itemView.click { listener.invoke(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseHolder {
        return ExerciseHolder(ItemExerciseGeneralBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return 40
    }

    override fun onBindViewHolder(holder: ExerciseHolder, position: Int) {
        holder.bind()
    }

}