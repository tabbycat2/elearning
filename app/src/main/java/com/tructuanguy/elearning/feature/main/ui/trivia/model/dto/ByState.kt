package com.tructuanguy.elearning.feature.main.ui.trivia.model.dto

data class ByState(
    val approved: Int,
    val pending: Int,
    val rejected: Int
)