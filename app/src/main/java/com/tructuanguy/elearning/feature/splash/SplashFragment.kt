package com.tructuanguy.elearning.feature.splash

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.constants.FirebaseConstants
import com.tructuanguy.elearning.databinding.FragmentSplashBinding
import com.tructuanguy.elearning.feature.log.ui.signin.SignInFragment
import com.tructuanguy.elearning.feature.log.ui.signin.SignInViewModel
import com.tructuanguy.elearning.feature.main.MainFragment
import com.tructuanguy.elearning.feature.main.ui.home.HomeFragment
import com.tructuanguy.elearning.feature.onboarding.OnboardingFragment
import com.tructuanguy.elearning.helper.PreferenceHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashFragment : BaseFragmentWithBinding<FragmentSplashBinding>() {

    @Inject
    lateinit var mPrefs: PreferenceHelper

    private val signInViewModel: SignInViewModel by viewModels()
    private val splashViewModel: SplashViewModel by viewModels()

    override fun getViewBinding(inflater: LayoutInflater): FragmentSplashBinding {
        return FragmentSplashBinding.inflate(layoutInflater)
    }

    override fun init() {

    }

    override fun initData() {
        signInViewModel.signInState.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Loading -> {
                    showLoadingDialog()
                }

                is DataState.Success -> {
                    hideLoadingDialog()
                    signInViewModel.getUserData()
                }

                is DataState.Error -> {
                    hideLoadingDialog()
                }
            }
        }
        signInViewModel.getUserDataState.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Success<User> -> {
                    hideLoadingDialog()
                    val bundle = Bundle()
                    bundle.putSerializable(FirebaseConstants.USER, it.data)
                    openFragment(HomeFragment::class.java, bundle, false)
                }

                is DataState.Error -> {
                    hideLoadingDialog()
                }

                is DataState.Loading -> {
                    showLoadingDialog()
                }
            }
        }
    }

    override fun initAction() {
        if (!mPrefs.isFirstInApp()) {
            Handler(Looper.getMainLooper()).postDelayed({
                openFragment(OnboardingFragment::class.java, null, false)
                mPrefs.saveFirstIn(true)
            }, 2000)
        } else {
            if (splashViewModel.getCurrentEmail().isNotEmpty()) {
                Handler(Looper.getMainLooper()).postDelayed({
                    signInViewModel.signIn(
                        splashViewModel.getCurrentEmail(),
                        splashViewModel.getCurrentPassword()
                    )
                }, 2000)
            } else {
                Handler(Looper.getMainLooper()).postDelayed({
                    hideLoadingDialog()
                    openFragment(SignInFragment::class.java, null, false)
                }, 2000)
            }
        }
    }


}