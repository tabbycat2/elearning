package com.tructuanguy.elearning.feature.main.ui.exercise

import android.app.usage.UsageStatsManager
import android.view.LayoutInflater
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.databinding.FragmentExerciseGeneralBinding
import com.tructuanguy.elearning.extension.click

class ExerciseGeneralFragment : BaseFragmentWithBinding<FragmentExerciseGeneralBinding>() {
    override fun getViewBinding(inflater: LayoutInflater): FragmentExerciseGeneralBinding {
        return FragmentExerciseGeneralBinding.inflate(layoutInflater)
    }

    override fun init() {

        UsageStatsManager.INTERVAL_DAILY

        binding.rcvExercise.adapter = ExerciseAdapter() {
            openFragment(DetailExerciseFragment::class.java, null, true, R.id.fr_app)
        }
    }

    override fun initData() {

    }

    override fun initAction() {
        binding.icBack.click { onBackPressed() }
    }


}