package com.tructuanguy.elearning.feature.main.ui.trivia.ui.category

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.main.ui.trivia.domain.usecase.GetMetaData
import com.tructuanguy.elearning.feature.main.ui.trivia.model.dto.MetaData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TriviaCategoryViewModel @Inject constructor(
    private val getMetaDataUseCase: GetMetaData
) : ViewModel() {

    private val _getMetaDataState = MutableLiveData<DataState<MetaData>>()
    val getMetaDataState: LiveData<DataState<MetaData>>
        get() = _getMetaDataState

    fun getMetaData() {
        Log.i("vvv", "@@@")
        viewModelScope.launch(Dispatchers.IO) {
            getMetaDataUseCase().onEach {
                _getMetaDataState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun getCategoryImage(): ArrayList<Int> {
        val list = arrayListOf<Int>()
        list.add(R.drawable.arts_literature)
        list.add(R.drawable.film)
        list.add(R.drawable.food_drink)
        list.add(R.drawable.general)
        list.add(R.drawable.geography)
        list.add(R.drawable.history)
        list.add(R.drawable.music)
        list.add(R.drawable.science)
        list.add(R.drawable.culture)
        list.add(R.drawable.sport)
        return list
    }

    fun getCategoryNameMap(): MutableMap<String, String> {
        val categoryNameMap = mutableMapOf<String, String>()
        categoryNameMap["arts_and_literature"] = "Arts & Literature"
        categoryNameMap["film_and_tv"] = "Films & TV"
        categoryNameMap["food_and_drink"] = "Food & Drinks"
        categoryNameMap["general_knowledge"] = "General Knowledge"
        categoryNameMap["geography"] = "Geography"
        categoryNameMap["history"] = "History"
        categoryNameMap["music"] = "Music"
        categoryNameMap["science"] = "Science"
        categoryNameMap["society_and_culture"] = "Society & Culture"
        categoryNameMap["sport_and_leisure"] = "Sports"
        return categoryNameMap
    }
}