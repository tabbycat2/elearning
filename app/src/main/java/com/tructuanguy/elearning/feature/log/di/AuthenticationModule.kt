package com.tructuanguy.elearning.feature.log.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.storage.FirebaseStorage
import com.tructuanguy.elearning.di.FirebaseModule
import com.tructuanguy.elearning.feature.log.domain.repository.AuthenticationRepository
import com.tructuanguy.elearning.feature.log.domain.data.AuthenticationRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AuthenticationModule {

    @Singleton
    @Provides
    fun provideAuthenticationRepository(
        auth: FirebaseAuth,
        @FirebaseModule.UserCollection userCollection: CollectionReference,
        storage: FirebaseStorage
    ): AuthenticationRepository {
        return AuthenticationRepositoryImpl(auth, userCollection, storage)
    }



}