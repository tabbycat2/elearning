package com.tructuanguy.elearning.feature.main.ui.trivia.model.dto

data class ByCategory(
    val arts_and_literature: Int,
    val film_and_tv: Int,
    val food_and_drink: Int,
    val general_knowledge: Int,
    val geography: Int,
    val history: Int,
    val music: Int,
    val science: Int,
    val society_and_culture: Int,
    val sport_and_leisure: Int
)