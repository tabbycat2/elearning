package com.tructuanguy.elearning.feature.main.ui.me.setting.ui.change_password

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.error.ChangePasswordError
import com.tructuanguy.elearning.error.PasswordEdittextError
import com.tructuanguy.elearning.extension.isValidPasswordLength
import com.tructuanguy.elearning.feature.log.domain.usecases.password.ChangePasswordUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChangePasswordViewModel @Inject constructor(
    private val changePasswordUseCase: ChangePasswordUseCase
) : ViewModel() {

    private val _errorOldPasswordState = MutableLiveData<PasswordEdittextError>()
    val errorOldPasswordState: LiveData<PasswordEdittextError>
        get() = _errorOldPasswordState

    private val _errorNewPasswordState = MutableLiveData<PasswordEdittextError>()
    val errorNewPasswordState: LiveData<PasswordEdittextError>
        get() = _errorNewPasswordState

    private val _errorConfirmPasswordState = MutableLiveData<PasswordEdittextError>()
    val errorConfirmPasswordState: LiveData<PasswordEdittextError>
        get() = _errorConfirmPasswordState

    private val _errorDiffFieldState = MutableLiveData<ChangePasswordError>()
    val errorDiffFieldState: LiveData<ChangePasswordError>
        get() = _errorDiffFieldState

    private val _changePasswordState = MutableLiveData<DataState<Boolean>>()
    val changePasswordState: LiveData<DataState<Boolean>>
        get() = _changePasswordState

    fun changePassword(email: String, oldPassword: String, newPassword: String) {
        viewModelScope.launch(Dispatchers.IO) {
            changePasswordUseCase(email, oldPassword, newPassword).onEach {
                _changePasswordState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun isOKForChangePassword(
        userPassword: String,
        oldPassword: String,
        newPassword: String,
        confirmPassword: String
    ): Boolean {
        if (oldPassword.isEmpty()) {
            _errorOldPasswordState.value = PasswordEdittextError.EMPTY
            return false
        } else {
            if (!isValidPasswordLength(oldPassword)) {
                _errorOldPasswordState.value = PasswordEdittextError.PASSWORD_LENGTH
                return false
            }
        }
        if (newPassword.isEmpty()) {
            _errorNewPasswordState.value = PasswordEdittextError.EMPTY
            return false
        } else {
            if (!isValidPasswordLength(newPassword)) {
                _errorNewPasswordState.value = PasswordEdittextError.PASSWORD_LENGTH
                return false
            }
        }
        if (confirmPassword.isEmpty()) {
            _errorConfirmPasswordState.value = PasswordEdittextError.EMPTY
            return false
        } else {
            if (!isValidPasswordLength(confirmPassword)) {
                _errorConfirmPasswordState.value = PasswordEdittextError.PASSWORD_LENGTH
                return false
            }
        }
        if (oldPassword.isNotEmpty() && newPassword.isNotEmpty() && confirmPassword.isNotEmpty()
            && isValidPasswordLength(oldPassword) && isValidPasswordLength(newPassword) && isValidPasswordLength(
                confirmPassword
            )
        ) {
            if (oldPassword != userPassword) {
                _errorDiffFieldState.value = ChangePasswordError.DIFF_OLD_USER_PASSWORD
                return false
            } else {
                if (oldPassword == newPassword) {
                    _errorDiffFieldState.value = ChangePasswordError.SAME_OLD_NEW_PASSWORD
                    return false
                } else if (newPassword != confirmPassword) {
                    _errorDiffFieldState.value = ChangePasswordError.DIFF_NEW_CONFIRM_PASSWORD
                    return false
                }
            }
        }
        return true
    }

}