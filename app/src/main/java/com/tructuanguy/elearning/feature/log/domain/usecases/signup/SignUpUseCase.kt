package com.tructuanguy.elearning.feature.log.domain.usecases.signup

import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.log.domain.repository.AuthenticationRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SignUpUseCase @Inject constructor(
    private val authenticationRepository: AuthenticationRepository
) {
    suspend operator fun invoke(email: String, password: String, name: String): Flow<DataState<User>> =
        authenticationRepository.signUp(email, password, name)
}