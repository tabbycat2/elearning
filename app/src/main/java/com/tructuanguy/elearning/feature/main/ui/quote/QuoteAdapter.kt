package com.tructuanguy.elearning.feature.main.ui.quote

import android.annotation.SuppressLint
import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.elearning.databinding.ItemQuoteBinding
import com.tructuanguy.elearning.db.entities.Celebrity
import com.tructuanguy.elearning.db.entities.KeyWordQuote
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.extension.visible

class QuoteAdapter(
    private val list: List<Celebrity>
) : RecyclerView.Adapter<QuoteAdapter.QuoteHolder>() {

    private val listKeyWordQuote: ArrayList<KeyWordQuote> = arrayListOf()
    private var listOriginal: ArrayList<String> = arrayListOf()
    private var listPhonetic: ArrayList<String> = arrayListOf()
    private var listType: ArrayList<String> = arrayListOf()
    private var listMean: ArrayList<String> = arrayListOf()

    inner class QuoteHolder(private val binding: ItemQuoteBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(quote: Celebrity) {
            listKeyWordQuote.clear()
            listOriginal = quote.words?.split("*") as ArrayList<String>
            listPhonetic = quote.phonetic?.split("*") as ArrayList<String>
            listType = quote.type?.split("*") as ArrayList<String>
            listMean = quote.means?.split("*") as ArrayList<String>
            for(i in 0 until listOriginal.size) {
                listKeyWordQuote.add(KeyWordQuote(listOriginal[i], listPhonetic[i], listType[i], listMean[i]))
            }
            binding.apply {
                rcvKeyWord.adapter = KeyWordQuoteAdapter(listKeyWordQuote)
                tvCeleEng.text = Html.fromHtml(quote.cele_eng, Html.FROM_HTML_MODE_COMPACT)
                if(quote.author!!.isNotEmpty()) {
                    tvAuthor.text = "--${quote.author}"
                }
                tvCelebVn.text = quote.cele_vn
                cvTranslate.click {
                    tvCelebVn.visible()
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuoteHolder {
        return QuoteHolder(ItemQuoteBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: QuoteHolder, position: Int) {
        holder.bind(list[position])
    }


}