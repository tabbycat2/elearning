package com.tructuanguy.elearning.feature.main.ui.exercise

import android.content.Context
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.databinding.ItemExerciseDetailBinding
import com.tructuanguy.elearning.db.entities.ExamQuestion
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.extension.gone
import com.tructuanguy.elearning.extension.visible
import org.w3c.dom.Text

@Suppress("DEPRECATED_IDENTITY_EQUALS")
class DetailExeAdapter(
    private val list: List<ExamQuestion>,
    private val context: Context
): PagerAdapter() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return list.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = inflater.inflate(R.layout.item_exercise_detail, container, false)
        container.addView(view)

        val title = view.findViewById<TextView>(R.id.title)
        val paragraph = view.findViewById<TextView>(R.id.paragraph)
        val question = view.findViewById<TextView>(R.id.question)
        val tvAnswerA = view.findViewById<TextView>(R.id.tv_answer_a)
        val tvAnswerB = view.findViewById<TextView>(R.id.tv_answer_b)
        val tvAnswerC = view.findViewById<TextView>(R.id.tv_answer_c)
        val tvAnswerD = view.findViewById<TextView>(R.id.tv_answer_d)
        val layoutA = view.findViewById<RelativeLayout>(R.id.layout_answerA)
        val layoutB = view.findViewById<RelativeLayout>(R.id.layout_answerB)
        val layoutC = view.findViewById<RelativeLayout>(R.id.layout_answerC)
        val layoutD = view.findViewById<RelativeLayout>(R.id.layout_answerD)
        val tvExplain = view.findViewById<TextView>(R.id.tv_explain)
        val tvDetailExplain = view.findViewById<TextView>(R.id.tv_detail_explain)
        val tvA = view.findViewById<TextView>(R.id.tv_a)
        val tvB = view.findViewById<TextView>(R.id.tv_b)
        val tvC = view.findViewById<TextView>(R.id.tv_c)
        val tvD = view.findViewById<TextView>(R.id.tv_d)
        title.text = list[position].title
        if(list[position].paragraph != null) {
            paragraph.text = Html.fromHtml(list[position].paragraph, Html.FROM_HTML_MODE_COMPACT)
        }
        if(list[position].content != null) {
            question.text = Html.fromHtml(list[position].content, Html.FROM_HTML_MODE_COMPACT)
        }
        val listAnswers = list[position].answers!!.split("∴")
        tvAnswerA.text = listAnswers[0]
        tvAnswerB.text = listAnswers[1]
        tvAnswerC.text = listAnswers[2]
        tvAnswerD.text = listAnswers[3]

        when(list[position].t_answer) {
            1 -> setChosenColor(tvA, tvAnswerA)
            2 -> setChosenColor(tvB, tvAnswerB)
            3 -> setChosenColor(tvC, tvAnswerC)
            4 -> setChosenColor(tvD, tvAnswerD)
            else -> {
                setDefaultColor(tvA, tvAnswerA)
                setDefaultColor(tvB, tvAnswerB)
                setDefaultColor(tvC, tvAnswerC)
                setDefaultColor(tvD, tvAnswerD)
            }
        }

        if(!DetailExerciseFragment.checkResult) {
            layoutA.click {
                list[position].t_answer = 1
                setChosenColor(tvA, tvAnswerA)
                setDefaultColor(tvB, tvAnswerB)
                setDefaultColor(tvC, tvAnswerC)
                setDefaultColor(tvD, tvAnswerD)
            }

            layoutB.click {
                list[position].t_answer = 2
                setChosenColor(tvB, tvAnswerB)
                setDefaultColor(tvA, tvAnswerA)
                setDefaultColor(tvC, tvAnswerC)
                setDefaultColor(tvD, tvAnswerD)
            }

            layoutC.click {
                list[position].t_answer = 3
                setChosenColor(tvC, tvAnswerC)
                setDefaultColor(tvB, tvAnswerB)
                setDefaultColor(tvA, tvAnswerA)
                setDefaultColor(tvD, tvAnswerD)
            }

            layoutD.click {
                list[position].t_answer = 4
                setChosenColor(tvD, tvAnswerD)
                setDefaultColor(tvB, tvAnswerB)
                setDefaultColor(tvC, tvAnswerC)
                setDefaultColor(tvA, tvAnswerA)
            }
        } else {
            tvExplain.visible()
            tvDetailExplain.visible()
            tvDetailExplain.text = list[position].t_explain
            if(list[position].t_key == 1) setTrueColor(tvA, tvAnswerA) else if(list[position].t_key == 2) setTrueColor(tvB, tvAnswerB)
            else if(list[position].t_key == 3) setTrueColor(tvC, tvAnswerC) else if(list[position].t_key == 4) setTrueColor(tvD, tvAnswerD)

        }

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val view = `object` as View
        container.removeView(view)
    }

    private fun setChosenColor(tvIndex: TextView, tvAnswer: TextView) {
        tvIndex.background = AppCompatResources.getDrawable(context, R.drawable.circle_index_orange)
        tvAnswer.setTextColor(context.resources.getColor(R.color.orange, null))
    }

    private fun setDefaultColor(tvIndex: TextView, tvAnswer: TextView) {
        tvIndex.background = AppCompatResources.getDrawable(context, R.drawable.circle_index_default)
        tvAnswer.setTextColor(context.resources.getColor(R.color.neutral_black, null))
    }

    private fun setTrueColor(tvIndex: TextView, tvAnswer: TextView) {
        tvIndex.background = AppCompatResources.getDrawable(context, R.drawable.circle_index_green)
        tvAnswer.setTextColor(context.resources.getColor(R.color.green, null))
    }

}