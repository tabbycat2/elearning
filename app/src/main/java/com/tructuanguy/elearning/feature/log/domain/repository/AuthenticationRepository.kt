package com.tructuanguy.elearning.feature.log.domain.repository

import android.net.Uri
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.common.DataState
import kotlinx.coroutines.flow.Flow

interface AuthenticationRepository {

    suspend fun signUp(email: String, password: String, userName: String): Flow<DataState<User>>

    suspend fun saveUserFireStore(user: User): Flow<DataState<Boolean>>

    suspend fun signIn(email: String, password: String): Flow<DataState<Boolean>>

    suspend fun getUserData(): Flow<DataState<User>>

    suspend fun changePassword(email: String, oldPassword: String, newPassword: String): Flow<DataState<Boolean>>

    suspend fun forgotPassword(email: String): Flow<DataState<Boolean>>

    suspend fun logOut(): Flow<DataState<Boolean>>

    suspend fun updateUserImage(image: Uri): Flow<DataState<Boolean>>

}