package com.tructuanguy.elearning.feature.main.ui.trivia.model.api

import com.tructuanguy.elearning.feature.main.ui.trivia.model.dto.MetaData
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.model.TriviaItem
import retrofit2.http.GET
import retrofit2.http.Query

interface TriviaApi {

    @GET("v2/metadata")
    suspend fun getMetaData(): MetaData

    @GET("v2/questions")
    suspend fun getCategoryQuestion(@Query("categories") type: String) : ArrayList<TriviaItem>

}
