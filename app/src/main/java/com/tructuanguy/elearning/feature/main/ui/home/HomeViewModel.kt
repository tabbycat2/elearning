package com.tructuanguy.elearning.feature.main.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.main.ui.home.domain.usecase.GetAllUserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getAllUserUseCase: GetAllUserUseCase
) : ViewModel() {

    private val _getAllUserState = MutableLiveData<DataState<List<User>>>()
    val getAllUserState: LiveData<DataState<List<User>>>
        get() = _getAllUserState

    fun getAllUser() {
        viewModelScope.launch(Dispatchers.IO) {
            getAllUserUseCase().onEach {
                _getAllUserState.value = it
            }.launchIn(viewModelScope)
        }
    }

}