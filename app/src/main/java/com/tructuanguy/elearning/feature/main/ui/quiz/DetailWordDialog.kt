package com.tructuanguy.elearning.feature.main.ui.quiz

import android.graphics.Paint
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.databinding.DialogDetailWordBinding
import com.tructuanguy.elearning.db.entities.Word
import com.tructuanguy.elearning.extension.click
import java.util.Locale

class DetailWordDialog(
    private val listener: () -> Unit
) : DialogFragment() {

    private lateinit var binding: DialogDetailWordBinding
    private lateinit var vocab: Word
    private lateinit var tts: TextToSpeech
    private var language = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.full_screen_dialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogDetailWordBinding.inflate(layoutInflater)
        vocab = arguments?.getSerializable("quiz_word") as Word
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tts = TextToSpeech(requireContext()) {
            if (language == 1) {
                tts.setLanguage(Locale.UK)
            } else if (language == 2) {
                tts.setLanguage(Locale.US)
            }
        }
        binding.apply {
            val examplesEng = vocab.example_eng?.split("??")
            val examplesVn = vocab.example_vn?.split("??")
            Glide.with(requireContext()).load(vocab.image).into(ivWord)
            tvOriginal.text = vocab.word
            tvMean.text = vocab.mean
            tvSpellUk.text = vocab.spell_uk
            tvSpellUs.text = vocab.spell_us
            tvExplainEng.text = vocab.explain_eng
            tvExplainVn.text = vocab.explain_vn
            exampleEng1.text = examplesEng!![0]
            exampleVn1.text = examplesVn!![0]
            exampleEng2.text = examplesEng[1]
            exampleVn2.text = examplesVn[1]
            tvUrl.text = vocab.url
            tvUrl.paintFlags = Paint.UNDERLINE_TEXT_FLAG
            layoutSpellUk.click {
                language = 1
                tts.speak(vocab.word, TextToSpeech.QUEUE_FLUSH, null, "")
            }
            layoutSpellUs.click {
                language = 2
                tts.speak(vocab.word, TextToSpeech.QUEUE_FLUSH, null, "")
            }
            cvOk.click {
                dismiss()
                listener.invoke()
            }
        }
    }
}