package com.tructuanguy.elearning.feature.main.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Topic (
    @PrimaryKey(autoGenerate = true)
    val topicId: Int,
    val topicName: String,
    val image: String,
): Serializable