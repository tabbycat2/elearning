package com.tructuanguy.elearning.feature.main.ui.me.setting.ui.change_password

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.common.dialog.ResetAppDialog
import com.tructuanguy.elearning.constants.FirebaseConstants.USER
import com.tructuanguy.elearning.constants.PrefsConstants
import com.tructuanguy.elearning.databinding.FragmentChangePasswordBinding
import com.tructuanguy.elearning.error.ChangePasswordError
import com.tructuanguy.elearning.error.PasswordEdittextError
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.helper.PreferenceHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ChangePasswordFragment : BaseFragmentWithBinding<FragmentChangePasswordBinding>() {

    @Inject
    lateinit var mPrefs: PreferenceHelper

    private val changePasswordViewModel: ChangePasswordViewModel by viewModels()
    private lateinit var user: User

    override fun getViewBinding(inflater: LayoutInflater): FragmentChangePasswordBinding {
        return FragmentChangePasswordBinding.inflate(layoutInflater)
    }

    override fun init() {

        @Suppress("DEPRECATION")
        user = arguments?.getSerializable(USER) as User

        binding.cvChangePassword.click {
            if(changePasswordViewModel.isOKForChangePassword(
                userPassword = user.password,
                oldPassword = binding.edtOldPassword.text.toString().trim(),
                newPassword = binding.edtNewPassword.text.toString().trim(),
                confirmPassword = binding.edtConfirmNewPassword.text.toString().trim()
            )) {
                changePasswordViewModel.changePassword(
                    email = user.email,
                    oldPassword = binding.edtOldPassword.text.toString().trim(),
                    newPassword = binding.edtNewPassword.text.toString().trim()
                )
            }
        }
    }

    override fun initData() {
        changePasswordViewModel.changePasswordState.observe(viewLifecycleOwner) {
            when(it) {
                is DataState.Error -> {
                    hideLoadingDialog()
                }
                is DataState.Loading -> {
                    showLoadingDialog()
                }
                is DataState.Success -> {
                    hideLoadingDialog()
                    mPrefs.apply {
                        saveString(PrefsConstants.USER_EMAIL, "")
                        saveString(PrefsConstants.USER_PASSWORD, "")
                    }
                    val resetDialog = ResetAppDialog()
                    resetDialog.show(requireActivity().supportFragmentManager, this@ChangePasswordFragment.javaClass.name)
                }
            }
        }
        changePasswordViewModel.errorOldPasswordState.observe(viewLifecycleOwner) {
            when(it) {
                PasswordEdittextError.EMPTY -> notifyError(getString(R.string.empty_old_password))
                PasswordEdittextError.PASSWORD_LENGTH -> notifyError(getString(R.string.old_password_length))
                else -> {}
            }
        }
        changePasswordViewModel.errorNewPasswordState.observe(viewLifecycleOwner) {
            when(it) {
                PasswordEdittextError.EMPTY -> notifyError(getString(R.string.empty_new_password))
                PasswordEdittextError.PASSWORD_LENGTH -> notifyError(getString(R.string.new_password_length))
                else -> {}
            }
        }
        changePasswordViewModel.errorConfirmPasswordState.observe(viewLifecycleOwner) {
            when(it) {
                PasswordEdittextError.EMPTY -> notifyError(getString(R.string.empty_confirm_password))
                PasswordEdittextError.PASSWORD_LENGTH -> notifyError(getString(R.string.confirm_password_length))
                else -> {}
            }
        }
        changePasswordViewModel.errorDiffFieldState.observe(viewLifecycleOwner) {
            when(it) {
                ChangePasswordError.SAME_OLD_NEW_PASSWORD -> notifyError(getString(R.string.same_old_new_password))
                ChangePasswordError.DIFF_NEW_CONFIRM_PASSWORD -> notifyError(getString(R.string.diff_new_confirm_password))
                ChangePasswordError.DIFF_OLD_USER_PASSWORD -> notifyError(getString(R.string.user_password_wrong))
                else -> {}
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun notifyError(error: String) {
        hideLoadingDialog()
        binding.tvError.text = "*$error"
    }

    override fun initAction() {

    }


}