package com.tructuanguy.elearning.feature.main.ui.vocabulary

import android.graphics.Paint
import android.speech.tts.TextToSpeech
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tructuanguy.elearning.databinding.ItemVocabBinding
import com.tructuanguy.elearning.db.entities.Word
import com.tructuanguy.elearning.extension.click
import java.util.Locale

class DetailWordPagerAdapter(
    private val list: List<Word>
) : RecyclerView.Adapter<DetailWordPagerAdapter.DetailWordHolder>()  {

    private lateinit var tts: TextToSpeech
    private var language: Int = 0

    inner class DetailWordHolder(private val binding: ItemVocabBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(vocab: Word) {
            tts = TextToSpeech(itemView.context) {
                if (language == 1) {
                    tts.setLanguage(Locale.UK)
                } else if (language == 2) {
                    tts.setLanguage(Locale.US)
                }
            }
            binding.apply {
                val examplesEng = vocab.example_eng?.split("??")
                val examplesVn = vocab.example_vn?.split("??")
                Glide.with(itemView.context).load(vocab.image).into(ivWord)
                tvOriginal.text = vocab.word
                tvMean.text = vocab.mean
                tvSpellUk.text = vocab.spell_uk
                tvSpellUs.text = vocab.spell_us
                tvExplainEng.text = vocab.explain_eng
                tvExplainVn.text = vocab.explain_vn
                exampleEng1.text = examplesEng!![0]
                exampleVn1.text = examplesVn!![0]
                exampleEng2.text = examplesEng[1]
                exampleVn2.text = examplesVn[1]
                tvUrl.text = vocab.url
                tvUrl.paintFlags = Paint.UNDERLINE_TEXT_FLAG
                layoutSpellUk.click {
                    language = 1
                    tts.speak(vocab.word, TextToSpeech.QUEUE_FLUSH, null, "")
                }
                layoutSpellUs.click {
                    language = 2
                    tts.speak(vocab.word, TextToSpeech.QUEUE_FLUSH, null, "")
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailWordHolder {
        return DetailWordHolder(ItemVocabBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: DetailWordHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        tts.stop()
        tts.shutdown()
    }

}