package com.tructuanguy.elearning.feature.main.ui.trivia.ui.play

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.google.android.material.card.MaterialCardView
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.constants.FirebaseConstants.USER
import com.tructuanguy.elearning.databinding.FragmentPlayTriviaBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.category.TRIVIA_CATEGORY
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.model.TriviaItem
import dagger.hilt.android.AndroidEntryPoint
import kotlin.random.Random

@AndroidEntryPoint
class PlayTriviaFragment : BaseFragmentWithBinding<FragmentPlayTriviaBinding>() {

    private val playTriviaViewModel: PlayTriviaViewModel by viewModels()
    private lateinit var user: User
    private var currentQuestion = 0
    private var triviaCategory = ""
    private var listTrivia: ArrayList<TriviaItem> = arrayListOf()
    private lateinit var countDownTimer: CountDownTimer
    private var isDown = true
    private lateinit var handleAnswered: Handler
    private lateinit var handleNextQuestion: Handler
    private var isUsedSupportBot = false
    private var isUsed5050 = false



    override fun getViewBinding(inflater: LayoutInflater): FragmentPlayTriviaBinding {
        return FragmentPlayTriviaBinding.inflate(layoutInflater)
    }

    override fun init() {
        user = arguments?.getSerializable(USER) as User
        Log.i("FFF", user.toString())
        triviaCategory = arguments?.getString(TRIVIA_CATEGORY).toString()
        handleAnswered = Handler(Looper.myLooper()!!)
        handleNextQuestion = Handler(Looper.myLooper()!!)
        playTriviaViewModel.getTriviaCategory(triviaCategory)
    }

    @SuppressLint("SetTextI18n")
    override fun initData() {
        playTriviaViewModel.getTriviaCategoryState.observe(viewLifecycleOwner) { state ->
            when (state) {
                is DataState.Error -> {
                    hideLoadingDialog()
                    Log.i("error", state.message)
                }

                is DataState.Loading -> {
                    showLoadingDialog()
                }

                is DataState.Success -> {
                    Log.i("success", state.data.toString())
                    hideLoadingDialog()
                    listTrivia = state.data as ArrayList<TriviaItem>
                    fillData(listTrivia[currentQuestion])
                    binding.tvCurrentQuestion.text = "$currentQuestion/10"
                    countdown()
                    playGame(listTrivia[currentQuestion])
                    binding.spBot.click {
                        if(isUsedSupportBot) {
                            toast("used")
                            return@click
                        } else {
                            isUsedSupportBot = true
                            binding.spBot.setImageResource(R.drawable.bot_clicked)
                            showSupportBotDialog()
                        }
                    }
                    binding.sp5050.click {
                        if(isUsed5050) {
                            toast("used")
                            return@click
                        } else {
                            binding.sp5050.setImageResource(R.drawable.fifty_fifty_clicked)
                            sp5050Clicked()
                        }
                    }
                }
            }
            playTriviaViewModel.updateUserExpState.observe(viewLifecycleOwner) {
                when(it) {
                    is DataState.Error -> {
                        Log.i("error", it.message)
                    }
                    is DataState.Loading -> {

                    }
                    is DataState.Success -> {
                        showWinDialog()
                    }
                }
            }
        }
    }

    private fun playGame(trivia: TriviaItem) {
        chooseAnswer(binding.cvAnsB, binding.cvAnsC, binding.cvAnsD, binding.cvAnsA, trivia.correctAnswer)
        chooseAnswer(binding.cvAnsA, binding.cvAnsC, binding.cvAnsD, binding.cvAnsB, trivia.correctAnswer)
        chooseAnswer(binding.cvAnsA, binding.cvAnsB, binding.cvAnsD, binding.cvAnsC, trivia.correctAnswer)
        chooseAnswer(binding.cvAnsA, binding.cvAnsB, binding.cvAnsC, binding.cvAnsD, trivia.correctAnswer)
    }

    private fun chooseAnswer(cv1: CardView, cv2: CardView, cv3: CardView, cvChosen: CardView, correctAnswer: String) {
        var chosen = ""
        cvChosen.click {
            when(cvChosen) {
                binding.cvAnsA -> {
                    chosen = binding.answerA.text.toString()
                    setColorChosen(binding.lnAnsA, binding.cvAnsA)
                    stopTimer()
                }
                binding.cvAnsB -> {
                    chosen = binding.answerB.text.toString()
                    setColorChosen(binding.lnAnsB, binding.cvAnsB)
                    stopTimer()
                }
                binding.cvAnsC -> {
                    chosen = binding.answerC.text.toString()
                    setColorChosen(binding.lnAnsC, binding.cvAnsC)
                    stopTimer()
                }
                binding.cvAnsD -> {
                    chosen = binding.answerD.text.toString()
                    setColorChosen(binding.lnAnsD, binding.cvAnsD)
                    stopTimer()
                }
            }

            cv1.isEnabled = false
            cv2.isEnabled = false
            cv3.isEnabled = false
            cvChosen.isEnabled = false

            Log.i("chosen", chosen)
            Log.i("chosen_correct", correctAnswer)

            handleAnswered.postDelayed({
                if(chosen == correctAnswer) {
                    when(cvChosen) {
                        binding.cvAnsA -> setColorCorrectAnswer(binding.lnAnsA, binding.cvAnsA)
                        binding.cvAnsB -> setColorCorrectAnswer(binding.lnAnsB, binding.cvAnsB)
                        binding.cvAnsC -> setColorCorrectAnswer(binding.lnAnsC, binding.cvAnsC)
                        binding.cvAnsD -> setColorCorrectAnswer(binding.lnAnsD, binding.cvAnsD)
                    }
                    if(currentQuestion < 2) {
                        nextQuestion()
                    } else {
                        win()
                    }
                } else {
                    when(cvChosen) {
                        binding.cvAnsA -> setColorWrongAnswer(binding.lnAnsA, binding.cvAnsA)
                        binding.cvAnsB -> setColorWrongAnswer(binding.lnAnsB, binding.cvAnsB)
                        binding.cvAnsC -> setColorWrongAnswer(binding.lnAnsC, binding.cvAnsC)
                        binding.cvAnsD -> setColorWrongAnswer(binding.lnAnsD, binding.cvAnsD)
                    }
                    when(correctAnswer) {
                        binding.answerA.text -> setColorCorrectAnswer(binding.lnAnsA, binding.cvAnsA)
                        binding.answerB.text -> setColorCorrectAnswer(binding.lnAnsB, binding.cvAnsB)
                        binding.answerC.text -> setColorCorrectAnswer(binding.lnAnsC, binding.cvAnsC)
                        binding.answerD.text -> setColorCorrectAnswer(binding.lnAnsD, binding.cvAnsD)
                    }
                    lose()
                }
            }, 2500)

        }
    }

    private fun reset() {
        resetColorAnswer(binding.lnAnsA, binding.cvAnsA)
        resetColorAnswer(binding.lnAnsB, binding.cvAnsB)
        resetColorAnswer(binding.lnAnsC, binding.cvAnsC)
        resetColorAnswer(binding.lnAnsD, binding.cvAnsD)
        binding.cvAnsA.isEnabled = true
        binding.cvAnsB.isEnabled = true
        binding.cvAnsC.isEnabled = true
        binding.cvAnsD.isEnabled = true
        binding.pbTime.progress = 0
    }

    private fun lose() {
        showLoseDialog()
    }

    private fun showLoseDialog() {
        val dialog = TriviaLoseDialog {
            onBackPressed()
        }
        dialog.show(childFragmentManager, this@PlayTriviaFragment.javaClass.simpleName)
    }

    private fun win() {
        playTriviaViewModel.updateUserExp(user.id, user.exp + 5)
    }



    private fun showWinDialog() {
        val dialog = TriviaWinDialog {
            onBackPressed()
        }
        dialog.show(childFragmentManager, this@PlayTriviaFragment.javaClass.simpleName)
    }

    private fun setColorChosen(lnChosen: LinearLayout, cvChosen: MaterialCardView) {
        lnChosen.background = ContextCompat.getDrawable(requireContext(), R.color.orange_lighter)
        cvChosen.strokeColor = ContextCompat.getColor(requireContext(), R.color.orange)
    }

    private fun setColorCorrectAnswer(lnChosen: LinearLayout, cvChosen: MaterialCardView) {
        lnChosen.background = ContextCompat.getDrawable(requireContext(), R.color.green_lighter)
        cvChosen.strokeColor = ContextCompat.getColor(requireContext(), R.color.green)
    }

    private fun resetColorAnswer(ln: LinearLayout, cv: MaterialCardView) {
        ln.background = ContextCompat.getDrawable(requireContext(), R.color.blue_lighter)
        cv.strokeColor = ContextCompat.getColor(requireContext(), R.color.blue_item)
    }

    private fun setColorWrongAnswer(lnChosen: LinearLayout, cvChosen: MaterialCardView) {
        lnChosen.background = ContextCompat.getDrawable(requireContext(), R.color.red_lighter)
        cvChosen.strokeColor = ContextCompat.getColor(requireContext(), R.color.red_setting)
    }

    override fun initAction() {
        binding.icBack.click {
            onBackPressed()
        }
    }

    private fun showSupportBotDialog() {
        val dialog = BotSupportDialog()
        val bundle = Bundle()
        bundle.putStringArrayList("answer", arrayListOf(
            binding.answerA.text.toString(),
            binding.answerB.text.toString(),
            binding.answerC.text.toString(),
            binding.answerD.text.toString(),
            listTrivia[currentQuestion].correctAnswer
        ))
        dialog.arguments = bundle
        dialog.show(childFragmentManager, "sp_bot")
    }

    private fun fillData(trivia: TriviaItem) {
        val listInCorrectAnswer = trivia.incorrectAnswers
        val listAnswer = arrayListOf<String>()
        listAnswer.add(listInCorrectAnswer[0])
        listAnswer.add(listInCorrectAnswer[1])
        listAnswer.add(listInCorrectAnswer[2])
        listAnswer.add(trivia.correctAnswer)
        listAnswer.shuffle()
        binding.apply {
            tvQuestion.text = trivia.question.text
            answerA.text = listAnswer[0]
            answerB.text = listAnswer[1]
            answerC.text = listAnswer[2]
            answerD.text = listAnswer[3]
        }
    }

    @SuppressLint("SetTextI18n")
    private fun nextQuestion() {
        handleNextQuestion.postDelayed({
            reset()
            binding.pbQuestion.progress = binding.pbQuestion.progress + 1
            currentQuestion++
            binding.tvCurrentQuestion.text = "$currentQuestion/10"
            countdown(30000)
            fillData(listTrivia[currentQuestion])
            playGame(listTrivia[currentQuestion])
        }, 2000)
    }

    private fun sp5050Clicked() {
        isUsed5050 = true
        fiftyFiftySupport(binding.answerA, binding.answerB, binding.answerC, binding.answerD)
        fiftyFiftySupport(binding.answerB, binding.answerA, binding.answerC, binding.answerD)
        fiftyFiftySupport(binding.answerC, binding.answerA, binding.answerB, binding.answerD)
        fiftyFiftySupport(binding.answerD, binding.answerA, binding.answerB, binding.answerC)
    }

    private fun fiftyFiftySupport(
        tvTrueAnswer: TextView,
        tv1: TextView,
        tv2: TextView,
        tv3: TextView
    ) {
        val rd = Random.nextInt(1, 4)
        if (tvTrueAnswer.text.toString() == listTrivia[currentQuestion].correctAnswer) {
            when (rd) {
                1 -> {
                    tv1.isEnabled = false
                    tv2.isEnabled = false
                    tv1.text = ""
                    tv2.text = ""
                }
                2 -> {
                    tv2.isEnabled = false
                    tv3.isEnabled = false
                    tv2.text = ""
                    tv3.text = ""
                }
                3 -> {
                    tv1.isEnabled = false
                    tv3.isEnabled = false
                    tv1.text = ""
                    tv3.text = ""
                }
            }
        }
    }

    private fun countdown(time: Long = 30000) {
        if(isDown) {
            countDownTimer = object : CountDownTimer(time, 1000) {
                override fun onTick(p0: Long) {
                    isDown = true
                    binding.counter.text = (p0 / 1000).toString()
                    binding.pbTime.progress = binding.pbTime.progress + 1
                }

                override fun onFinish() {
                    lose()
                }
            }.start()
        } else {
            isDown = false
        }
    }

    private fun stopTimer() {
        countDownTimer.cancel()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        stopTimer()
    }


}