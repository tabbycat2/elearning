package com.tructuanguy.elearning.feature.main.ui.exercise

import android.view.LayoutInflater
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.databinding.FragmentDetailExerciseBinding

class AnsweredExeDialog : BaseFragmentWithBinding<FragmentDetailExerciseBinding>() {
    override fun getViewBinding(inflater: LayoutInflater): FragmentDetailExerciseBinding {
        return FragmentDetailExerciseBinding.inflate(layoutInflater)
    }

    override fun init() {

    }

    override fun initData() {
    }

    override fun initAction() {
    }


}