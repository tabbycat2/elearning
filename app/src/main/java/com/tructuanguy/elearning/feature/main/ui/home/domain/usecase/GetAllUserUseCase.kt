package com.tructuanguy.elearning.feature.main.ui.home.domain.usecase

import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.main.ui.home.domain.repository.MainHomeRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllUserUseCase @Inject constructor(
    private val mainHomeRepository: MainHomeRepository
) {
    suspend operator fun invoke(): Flow<DataState<List<User>>> = mainHomeRepository.getAllUser()
}