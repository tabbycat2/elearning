package com.tructuanguy.elearning.feature.main.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tructuanguy.elearning.databinding.ItemTopicHorizontalBinding
import com.tructuanguy.elearning.feature.main.model.Topic

class VocabularyTopicAdapter(
    private val list: List<Topic>,
    private val listener: (Topic, Int) -> Unit
) : RecyclerView.Adapter<VocabularyTopicAdapter.TopicHolder>() {

    inner class TopicHolder(private val binding: ItemTopicHorizontalBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(topic: Topic) {
            binding.apply {
                tvTopicName.text = topic.topicName
                Glide.with(itemView).load(topic.image).into(ivTopic)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicHolder {
        return TopicHolder(ItemTopicHorizontalBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TopicHolder, position: Int) {
        holder.bind(list[position])
    }


}