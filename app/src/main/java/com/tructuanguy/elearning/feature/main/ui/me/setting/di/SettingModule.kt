package com.tructuanguy.elearning.feature.main.ui.me.setting.di

import com.tructuanguy.elearning.db.AppDB
import com.tructuanguy.elearning.feature.main.ui.me.setting.domain.data.SettingRepositoryImpl
import com.tructuanguy.elearning.feature.main.ui.me.setting.domain.repository.SettingRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SettingModule {

    @Provides
    @Singleton
    fun provideSettingRepository(db: AppDB): SettingRepository {
        return SettingRepositoryImpl(db)
    }

}