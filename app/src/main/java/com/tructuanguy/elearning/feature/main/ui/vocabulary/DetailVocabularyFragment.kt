package com.tructuanguy.elearning.feature.main.ui.vocabulary

import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.databinding.FragmentDetailVocabBinding
import com.tructuanguy.elearning.feature.main.model.Topic
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailVocabularyFragment : BaseFragmentWithBinding<FragmentDetailVocabBinding>() {

    private val vocabViewModel: VocabularyViewModel by viewModels()
    private lateinit var topic: Topic

    override fun getViewBinding(inflater: LayoutInflater): FragmentDetailVocabBinding {
        return FragmentDetailVocabBinding.inflate(layoutInflater)
    }

    override fun init() {
        topic = arguments?.getSerializable("topic") as Topic
        vocabViewModel.getWordByTopicId(topic.topicId)
    }

    override fun initData() {
        vocabViewModel.getWordByTopicIdState.observe(viewLifecycleOwner) { state ->
            when(state) {
                is DataState.Error -> {
                    Log.i("error", state.message)
                }
                is DataState.Loading -> {

                }
                is DataState.Success -> {
                    val adapter = DetailWordPagerAdapter(state.data)
                    binding.vpgVocab.adapter = adapter
                }
            }
        }
    }

    override fun initAction() {

    }


}