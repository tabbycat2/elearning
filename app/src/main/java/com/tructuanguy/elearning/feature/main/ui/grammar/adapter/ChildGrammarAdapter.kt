package com.tructuanguy.elearning.feature.main.ui.grammar.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.elearning.databinding.ItemChildGrammarBinding
import com.tructuanguy.elearning.db.entities.Grammar
import com.tructuanguy.elearning.extension.click

class ChildGrammarAdapter(
    private val list: List<Grammar>,
    private val listener: (Grammar) -> Unit
) : RecyclerView.Adapter<ChildGrammarAdapter.ChildGrammarHolder>() {

    inner class ChildGrammarHolder(private val binding: ItemChildGrammarBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(grammar: Grammar) {
            binding.tvMean.text = grammar.mean
            binding.tvOriginal.text = grammar.name
            itemView.click {
                listener.invoke(grammar)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildGrammarHolder {
        return ChildGrammarHolder(ItemChildGrammarBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ChildGrammarHolder, position: Int) {
        holder.bind(list[position])
    }


}