package com.tructuanguy.elearning.feature.onboarding

import android.content.Context
import androidx.core.content.ContextCompat.getString
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.di.AppModule
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext

import javax.inject.Inject

@HiltViewModel
class OnBoardingViewModel @Inject constructor(
    private val resourceProvider: AppModule.ResourceProvider
) : ViewModel() {

    val listOnboardingImages = MutableLiveData<ArrayList<OnBoarding>>()
    private val mListOnBoardingImages = ArrayList<OnBoarding>()

    fun getOnBoardingImages() {
        mListOnBoardingImages.add(
            OnBoarding(
                R.drawable.onboarding1,
                resourceProvider.getString(R.string.title_onboarding1),
                resourceProvider.getString(R.string.description_onboarding1)
            )
        )
        mListOnBoardingImages.add(
            OnBoarding(
                R.drawable.onboarding2,
                resourceProvider.getString(R.string.title_onboarding2),
                resourceProvider.getString(R.string.description_onboarding2)
            )
        )
        mListOnBoardingImages.add(
            OnBoarding(
                R.drawable.onboarding3,
                resourceProvider.getString(R.string.title_onboarding3),
                resourceProvider.getString(R.string.description_onboarding3)
            )
        )
        listOnboardingImages.value = mListOnBoardingImages
    }

}