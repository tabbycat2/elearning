package com.tructuanguy.elearning.feature.main.ui.trivia.ui.model

import java.io.Serializable

data class TriviaCategory(
    val name: String,
    val size: Int,
    val image: Int
): Serializable
