package com.tructuanguy.elearning.feature.main.ui.exercise

import android.view.LayoutInflater
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.databinding.FragmentExerciseBinding
import com.tructuanguy.elearning.databinding.FragmentExerciseGeneralBinding
import com.tructuanguy.elearning.extension.click

class ExerciseFragment : BaseFragmentWithBinding<FragmentExerciseBinding>() {
    override fun getViewBinding(inflater: LayoutInflater): FragmentExerciseBinding {
        return FragmentExerciseBinding.inflate(layoutInflater)
    }

    override fun init() {

    }

    override fun initData() {

    }

    override fun initAction() {
        binding.cvBasic.click {
            openFragment(
                ExerciseGeneralFragment::class.java, null, true, R.id.fr_app
            )
        }
        binding.icBack.click { onBackPressed() }
    }


}