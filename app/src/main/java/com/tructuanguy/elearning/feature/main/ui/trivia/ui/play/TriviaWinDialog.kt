package com.tructuanguy.elearning.feature.main.ui.trivia.ui.play

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.databinding.DialogWinTriviaBinding
import com.tructuanguy.elearning.extension.click

class TriviaWinDialog(
    private val listener: () -> Unit
) : DialogFragment() {

    private lateinit var binding: DialogWinTriviaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.full_screen_dialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogWinTriviaBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.cvOk.click {
            dismiss()
            listener.invoke()
        }
    }

}