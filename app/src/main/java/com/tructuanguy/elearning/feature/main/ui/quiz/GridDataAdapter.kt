package com.tructuanguy.elearning.feature.main.ui.quiz

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.tructuanguy.elearning.R

class GridDataAdapter(
    private val context: Context,
    private var arr: ArrayList<String>,
    private val type: Int
) : BaseAdapter() {
    override fun getCount(): Int {
        return arr.size
    }

    override fun getItem(p0: Int): Any {
        return arr[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    @SuppressLint("ViewHolder")
    override fun getView(pos: Int, convertView: View?, parent: ViewGroup?): View {
        lateinit var view: View
        if (type == 1) {
            view = View.inflate(context, R.layout.item_grid_quiz_blue, null)
            val tvChar: TextView = view.findViewById(R.id.tv_character)

            val cell: String = arr[pos]
            tvChar.text = cell.uppercase()
        } else if(type == 2) {
            view = View.inflate(context, R.layout.item_grid_quiz_red, null)
            val tvChar: TextView = view.findViewById(R.id.tv_character)

            val cell: String = arr[pos]
            tvChar.text = cell.uppercase()
        }
        return view
    }
}