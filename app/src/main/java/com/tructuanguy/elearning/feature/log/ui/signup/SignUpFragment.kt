package com.tructuanguy.elearning.feature.log.ui.signup

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.google.android.material.textfield.TextInputLayout
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.common.EditTextError
import com.tructuanguy.elearning.constants.FirebaseConstants.USER
import com.tructuanguy.elearning.constants.FirebaseConstants.USER_EMAIL
import com.tructuanguy.elearning.constants.FirebaseConstants.USER_PASSWORD
import com.tructuanguy.elearning.databinding.FragmentSignUpBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.log.ui.signin.SignInFragment
import com.tructuanguy.elearning.feature.log.ui.signin.SignInViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpFragment : BaseFragmentWithBinding<FragmentSignUpBinding>() {

    private val signUpViewModel: SignUpViewModel by viewModels()
    private val signInViewModel: SignInViewModel by viewModels()
    private lateinit var user: User

    override fun getViewBinding(inflater: LayoutInflater): FragmentSignUpBinding {
        return FragmentSignUpBinding.inflate(layoutInflater)
    }

    override fun init() {

    }

    override fun initData() {
        signUpViewModel.signUpState.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Success<User> -> {
                    hideLoadingDialog()
                    user = it.data
                    signUpViewModel.saveUserFireStore(it.data)
                }

                is DataState.Error -> {
                    hideLoadingDialog()
                }

                is DataState.Loading -> {
                    showLoadingDialog()
                }
            }
        }
        signUpViewModel.saveUserFireStoreState.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Success<Boolean> -> {
                    hideLoadingDialog()
                    val bundle = Bundle()
                    bundle.putSerializable(USER, user)
                    openFragment(ProfileCreatedFragment::class.java, bundle, false)
                }

                is DataState.Error -> {
                    hideLoadingDialog()
                }

                is DataState.Loading -> {
                    showLoadingDialog()
                }
            }
        }
    }

    override fun initAction() {

        binding.tvSignInNow.click {
            openFragment(SignInFragment::class.java, null, false)
        }

        binding.apply {
            cvSignUp.click {
                if (signUpViewModel.isOkSignUp(
                        binding.edtEmail.text!!.trim().toString(),
                        binding.edtPassword.text!!.trim().toString(),
                        binding.edtName.text!!.trim().toString()
                    )
                ) {
                    signUpViewModel.signUp(
                        binding.edtEmail.text!!.trim().toString(),
                        binding.edtPassword.text!!.trim().toString(),
                        binding.edtName.text!!.trim().toString()
                    )
                } else {
                    signUpViewModel.editTextError.observe(viewLifecycleOwner) {
                        when (it) {
                            EditTextError.EMAIL_EMPTY -> notifyError(layoutEmail, getString(R.string.empty_email))
                            EditTextError.PASSWORD_EMPTY -> notifyError(layoutPassword, getString(R.string.empty_password))
                            EditTextError.USERNAME_EMPTY -> notifyError(layoutName, getString(R.string.empty_name))
                            EditTextError.EMAIL_FORMAT -> notifyError(layoutEmail, getString(R.string.email_format))
                            EditTextError.PASSWORD_LENGTH -> notifyError(layoutPassword, getString(R.string.password_length))
                            else -> {}
                        }
                    }
                }
            }
        }
    }

    private fun notifyError(
        layoutEdt: TextInputLayout,
        error: String
    ) {
        layoutEdt.error = error
        layoutEdt.boxStrokeColor = ContextCompat.getColor(requireContext(), R.color.red)
    }
}