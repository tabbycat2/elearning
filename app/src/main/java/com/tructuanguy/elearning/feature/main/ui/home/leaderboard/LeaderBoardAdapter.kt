package com.tructuanguy.elearning.feature.main.ui.home.leaderboard

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.databinding.ItemLeaderboardBinding
import okhttp3.internal.notifyAll

class LeaderBoardAdapter : ListAdapter<User, LeaderBoardAdapter.UserLeaderBoardHolder>(UserDiffCallback()){

    inner class UserLeaderBoardHolder(private val binding: ItemLeaderboardBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(user: User) {
            binding.apply {
                tvIndex.text = "${adapterPosition + 4}"
                tvUserName.text = user.userName
                if(user.image.isNotEmpty()) {
                    Glide.with(itemView.context).load(user.image).into(ivUser)
                } else {
                    ivUser.setImageResource(R.drawable.default_img_user)
                }
                binding.tvExp.text = "${user.exp} exp"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserLeaderBoardHolder {
        return UserLeaderBoardHolder(ItemLeaderboardBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: UserLeaderBoardHolder, position: Int) {
        holder.bind(getItem(position))
    }


}

class UserDiffCallback : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem == newItem
    }
}