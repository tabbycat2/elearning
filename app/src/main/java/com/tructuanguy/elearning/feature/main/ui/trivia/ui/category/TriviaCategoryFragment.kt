package com.tructuanguy.elearning.feature.main.ui.trivia.ui.category

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.constants.FirebaseConstants.USER
import com.tructuanguy.elearning.databinding.FragmentTriviaBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.ui.trivia.model.dto.ByCategory
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.model.TriviaCategory
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.play.PlayTriviaFragment
import dagger.hilt.android.AndroidEntryPoint

const val TRIVIA_CATEGORY = "trivia_category"

@AndroidEntryPoint
class TriviaCategoryFragment : BaseFragmentWithBinding<FragmentTriviaBinding>() {

    private val triviaCategoryViewModel: TriviaCategoryViewModel by viewModels()
    private var listCategoryImage: ArrayList<Int> = arrayListOf()
    private var listCategorySize: ArrayList<Int> = arrayListOf()
    private var listCategoryName: ArrayList<String> = arrayListOf()
    private var listCategory: ArrayList<TriviaCategory> = arrayListOf()
    private var mapCategoryName: MutableMap<String, String> = mutableMapOf()
    private lateinit var user: User

    override fun getViewBinding(inflater: LayoutInflater): FragmentTriviaBinding {
        return FragmentTriviaBinding.inflate(layoutInflater)
    }

    override fun init() {
        user = arguments?.getSerializable(USER) as User
        triviaCategoryViewModel.getMetaData()
        listCategoryImage = triviaCategoryViewModel.getCategoryImage()
        mapCategoryName = triviaCategoryViewModel.getCategoryNameMap()
    }

    override fun initData() {
        triviaCategoryViewModel.getMetaDataState.observe(viewLifecycleOwner) { state ->
            Log.i("bbb", "aaa")
            when (state) {
                is DataState.Error -> {
                    hideLoadingDialog()
                    Log.i("error", state.message)
                }

                is DataState.Loading -> {
                    showLoadingDialog()
                }

                is DataState.Success -> {
                    hideLoadingDialog()
                    val metadata = state.data
                    val category = metadata.byCategory
                    val categoryProperties = ByCategory::class.java.declaredFields
                    for (properties in categoryProperties) {
                        properties.isAccessible = true
                        val name: String = properties.name
                        val value = properties.get(category)
                        listCategoryName.add(name)
                        listCategorySize.add(value as Int)
                    }
                    for (i in 0 until listCategoryName.size) {
                        listCategory.add(
                            TriviaCategory(
                                name = mapCategoryName[listCategoryName[i]].toString(),
                                size = listCategorySize[i],
                                image = listCategoryImage[i]
                            )
                        )
                    }
                    listCategory.add(TriviaCategory("Custom", 0, R.drawable.custom))
                    binding.rcvCategoryTrivia.adapter =
                        TriviaCategoryAdapter(listCategory) { trivia ->
                            if (trivia.name != "Custom") {
                                val bundle = Bundle()
                                bundle.putSerializable(
                                    TRIVIA_CATEGORY,
                                    mapCategoryName.filterValues { it == trivia.name }.keys.toString()
                                        .replace("[", "")
                                        .replace("]", "")
                                )
                                bundle.putSerializable(USER, user)
                                binding.layoutTrivia.isEnabled = false
                                openFragment(
                                    PlayTriviaFragment::class.java,
                                    bundle,
                                    true,
                                    R.id.fr_app
                                )
                            }
                        }
                }
            }
        }
    }

    override fun initAction() {
        binding.icBack.click {
            onBackPressed()
        }
    }

    override fun onDestroyView() {
        Log.i("fragment", "destroy")
        super.onDestroyView()

    }



}