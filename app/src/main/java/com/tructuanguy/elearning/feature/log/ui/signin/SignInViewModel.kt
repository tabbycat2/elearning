package com.tructuanguy.elearning.feature.log.ui.signin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.common.EditTextError
import com.tructuanguy.elearning.db.domain.usecases.user_local.DeleteUserLocalUseCase
import com.tructuanguy.elearning.db.entities.UserLocal
import com.tructuanguy.elearning.extension.isValidEmail
import com.tructuanguy.elearning.extension.isValidPasswordLength
import com.tructuanguy.elearning.feature.log.domain.usecases.signin.GetUserDataUseCase
import com.tructuanguy.elearning.feature.log.domain.usecases.signin.LogOutUseCase
import com.tructuanguy.elearning.feature.log.domain.usecases.signin.SignInUseCase
import com.tructuanguy.elearning.db.domain.usecases.user_local.GetAllUserLocalUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val signInUseCase: SignInUseCase,
    private val getUserDataUseCase: GetUserDataUseCase,
    private val logOutUseCase: LogOutUseCase,
    private val getAllUserLocalUseCase: GetAllUserLocalUseCase,
    private val deleteUserLocalUseCase: DeleteUserLocalUseCase
) : ViewModel() {

    private val _editTextError: MutableLiveData<EditTextError> = MutableLiveData()
    val editTextError: LiveData<EditTextError>
        get() = _editTextError

    private val _signInState: MutableLiveData<DataState<Boolean>> = MutableLiveData()
    val signInState: LiveData<DataState<Boolean>>
        get() = _signInState

    private val _getUserDataState: MutableLiveData<DataState<User>> = MutableLiveData()
    val getUserDataState: LiveData<DataState<User>>
        get() = _getUserDataState

    private val _deleteUserLocalState: MutableLiveData<DataState<Boolean>> = MutableLiveData()
    val deleteUserLocalState: LiveData<DataState<Boolean>>
        get() = _deleteUserLocalState

    private val _logOutState: MutableLiveData<DataState<Boolean>> = MutableLiveData()
    val logOutState: LiveData<DataState<Boolean>>
        get() = _logOutState

    private val _getAllUserLocalState = MutableLiveData<DataState<List<UserLocal>>>()
    val getAllUserLocalState: LiveData<DataState<List<UserLocal>>>
        get() = _getAllUserLocalState

    fun getAllUserLocal() {
        viewModelScope.launch(Dispatchers.IO) {
            getAllUserLocalUseCase().onEach {
                _getAllUserLocalState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun deleteUserLocal(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            deleteUserLocalUseCase(id).onEach {
                _deleteUserLocalState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun signIn(email: String, password: String) {
        viewModelScope.launch {
            signInUseCase(email, password).onEach {
                _signInState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun getUserData() {
        viewModelScope.launch {
            getUserDataUseCase().onEach {
                _getUserDataState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun logOut() {
        viewModelScope.launch {
            logOutUseCase().onEach {
                _logOutState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun isOkForSignIn(email: String, password: String): Boolean {
        if(email.isEmpty()) {
            _editTextError.value = EditTextError.EMAIL_EMPTY
            return false
        } else if(email.isNotEmpty() && !isValidEmail(email)) {
            _editTextError.value = EditTextError.EMAIL_FORMAT
            return false
        } else if(password.isEmpty()) {
            _editTextError.value = EditTextError.PASSWORD_EMPTY
            return false
        } else if(password.isNotEmpty() && !isValidPasswordLength(password)) {
            _editTextError.value = EditTextError.PASSWORD_LENGTH
            return false
        }
        return true
    }

}