package com.tructuanguy.elearning.feature.log.domain.data

import android.net.Uri
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.SetOptions
import com.google.firebase.storage.FirebaseStorage
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.constants.FirebaseConstants.PASSWORD
import com.tructuanguy.elearning.constants.FirebaseConstants.USERS_IMAGE_STORAGE
import com.tructuanguy.elearning.di.FirebaseModule
import com.tructuanguy.elearning.feature.log.domain.repository.AuthenticationRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class AuthenticationRepositoryImpl @Inject constructor(
    private val auth: FirebaseAuth,
    @FirebaseModule.UserCollection private val userCollection: CollectionReference,
    private val storage: FirebaseStorage
) : AuthenticationRepository {
    override suspend fun signUp(
        email: String,
        password: String,
        userName: String
    ): Flow<DataState<User>> {
        return flow {
            emit(DataState.Loading())
            val result = suspendCoroutine<DataState<User>> { continuation ->
                try {
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnSuccessListener { auth ->
                            val user = User(
                                id = auth.user!!.uid,
                                userName = userName,
                                email = email,
                                password = password,
                                image = "",
                                exp = 0
                            )
                            continuation.resume(DataState.Success(user))
                        }
                        .addOnFailureListener {
                            continuation.resume(DataState.Error(it.toString()))
                        }
                } catch (e: Exception) {
                    continuation.resume(DataState.Error(e.toString()))
                }
            }
            emit(result)
        }
    }

    override suspend fun saveUserFireStore(user: User): Flow<DataState<Boolean>> =
        flow {
            var firebaseException = ""
            emit(DataState.Loading())
            try {
                var isSave = false
                userCollection.document(user.id).set(user, SetOptions.merge())
                    .addOnSuccessListener {
                        isSave = true
                    }
                    .addOnFailureListener {
                        isSave = false
                        firebaseException = it.toString()
                    }.await()
                emit(DataState.Success(isSave))
                emit(DataState.Error(firebaseException))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }

    override suspend fun signIn(email: String, password: String): Flow<DataState<Boolean>> {
        return flow {
            var firebaseException = ""
            emit(DataState.Loading())
            try {
                var isSuccess = false
                auth.signInWithEmailAndPassword(email, password)
                    .addOnSuccessListener { isSuccess = true }
                    .addOnFailureListener {
                        isSuccess = false
                        firebaseException = it.toString()
                    }.await()
                emit(DataState.Success(isSuccess))
                emit(DataState.Error(firebaseException))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getUserData(): Flow<DataState<User>> {
        return flow {
            emit(DataState.Loading())
            val user = auth.currentUser
            val result = suspendCoroutine<DataState<User>> { continuation ->
                try {
                    userCollection.document(user!!.uid).get()
                        .addOnSuccessListener {
                            val result = it.toObject(User::class.java)
                            continuation.resume(DataState.Success(result!!))
                        }
                        .addOnFailureListener {
                            continuation.resume(DataState.Error(it.toString()))
                        }
                } catch (e: Exception) {
                    continuation.resume(DataState.Error(e.toString()))
                }
            }
            emit(result)
        }
    }

    override suspend fun changePassword(
        email: String,
        oldPassword: String,
        newPassword: String
    ): Flow<DataState<Boolean>> {
        return flow {
            emit(DataState.Loading())
            val result = suspendCoroutine<DataState<Boolean>> { continuation ->
                try {
                    val currentUser = auth.currentUser
                    val credential = EmailAuthProvider.getCredential(email, oldPassword)
                    currentUser!!.reauthenticate(credential).addOnSuccessListener {
                        currentUser.updatePassword(newPassword).addOnSuccessListener {
                            userCollection.document(currentUser.uid)
                                .update(PASSWORD, newPassword)
                                .addOnSuccessListener {
                                    continuation.resume(DataState.Success(true))
                                }
                                .addOnFailureListener {
                                    continuation.resume(DataState.Error(it.toString()))
                                }
                        }
                    }
                } catch (e: Exception) {
                    continuation.resume(DataState.Error(e.toString()))
                }
            }
            emit(result)
        }
    }

    override suspend fun forgotPassword(email: String): Flow<DataState<Boolean>> {
        return flow {
            emit(DataState.Loading())
            val result = suspendCoroutine<DataState<Boolean>> { continuation ->
                try {
                    auth.sendPasswordResetEmail(email)
                        .addOnSuccessListener {
                            continuation.resume(DataState.Success(true))
                        }
                        .addOnFailureListener {
                            continuation.resume(DataState.Error(it.toString()))
                        }
                } catch (e: Exception) {
                    continuation.resume(DataState.Error(e.toString()))
                }
            }
            emit(result)
        }
    }

    override suspend fun logOut(): Flow<DataState<Boolean>> {
        return flow {
            emit(DataState.Loading())
            try {
                auth.signOut()
                emit(DataState.Success(true))
            } catch (e: Exception) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun updateUserImage(image: Uri): Flow<DataState<Boolean>> {
        return flow {
            emit(DataState.Loading())
            val result = suspendCoroutine<DataState<Boolean>> { continuation ->
                try {
                    val currentUser = auth.currentUser
                    val userImageRef =
                        storage.getReference(USERS_IMAGE_STORAGE).child(currentUser!!.uid)
                    userImageRef.putFile(image).addOnSuccessListener {
                        userImageRef.downloadUrl.addOnSuccessListener { uri ->
                            userCollection.document(currentUser.uid)
                                .update("image", uri.toString())
                                .addOnSuccessListener {
                                    continuation.resume(DataState.Success(true))
                                }
                                .addOnFailureListener {
                                    continuation.resume(DataState.Error(it.toString()))
                                }
                        }
                    }.addOnFailureListener {
                        continuation.resume(DataState.Error(it.toString()))
                    }
                } catch (e: Exception) {
                    continuation.resume(DataState.Error(e.toString()))
                }
            }
            emit(result)
        }
    }
}