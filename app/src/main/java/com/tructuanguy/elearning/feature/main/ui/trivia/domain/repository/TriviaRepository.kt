package com.tructuanguy.elearning.feature.main.ui.trivia.domain.repository

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.main.ui.trivia.model.dto.MetaData
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.model.TriviaItem
import kotlinx.coroutines.flow.Flow

interface TriviaRepository {

    suspend fun getMetaData(): Flow<DataState<MetaData>>

    suspend fun getCategoryQuiz(category: String): Flow<DataState<List<TriviaItem>>>

    suspend fun updateUserExp(id: String, exp: Int): Flow<DataState<Boolean>>

}