package com.tructuanguy.elearning.feature.main.ui.home

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.constants.FirebaseConstants.USER
import com.tructuanguy.elearning.databinding.FragmentHomeBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.ui.boormark.BookMarkFragment
import com.tructuanguy.elearning.feature.main.ui.exercise.ExerciseFragment
import com.tructuanguy.elearning.feature.main.ui.grammar.GrammarFragment
import com.tructuanguy.elearning.feature.main.ui.home.leaderboard.LeaderBoardAdapter
import com.tructuanguy.elearning.feature.main.ui.me.MeFragment
import com.tructuanguy.elearning.feature.main.ui.quiz.QuizFragment
import com.tructuanguy.elearning.feature.main.ui.quote.QuoteFragment
import com.tructuanguy.elearning.feature.main.ui.story.StoryFragment
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.category.TriviaCategoryFragment
import com.tructuanguy.elearning.feature.main.ui.vocabulary.VocabularyFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragmentWithBinding<FragmentHomeBinding>() {

    private val homeViewModel: HomeViewModel by viewModels()
    private lateinit var user: User

    override fun getViewBinding(inflater: LayoutInflater): FragmentHomeBinding {
        return FragmentHomeBinding.inflate(layoutInflater)
    }

    @SuppressLint("SetTextI18n")
    override fun init() {
        user = arguments?.getSerializable(USER) as User
        binding.tvHelloUser.text = "${getString(R.string.hi)}, ${user.userName}"
        if(user.image.isNotEmpty()) {
            Glide.with(requireContext()).load(user.image).into(binding.ivCurrentUser)
        } else {
            binding.ivCurrentUser.setImageResource(R.drawable.default_img_user)
        }
        binding.ivCurrentUser.click {
            openFragment(MeFragment::class.java, createBundle(USER, user), true, R.id.fr_app)
        }
        homeViewModel.getAllUser()
    }

    override fun initData() {
        homeViewModel.getAllUserState.observe(viewLifecycleOwner) {
            when(it) {
                is DataState.Error -> {
                    Log.i("error", it.message)
                }
                is DataState.Loading -> {
                    toast("loading")
                }
                is DataState.Success -> {
                    val list = it.data as MutableList
                    val firstThreeUser = list.take(3)
                    list.removeAll(firstThreeUser)
                    if(list.size <= 17) {
                        val adapter = LeaderBoardAdapter()
                        adapter.submitList(list)
                        Log.i("@@", list.toString())
                        binding.rcv.adapter = adapter
                    } else {
                        val shownList = list.take(17)
                        val adapter = LeaderBoardAdapter()
                        adapter.submitList(shownList)
                        binding.rcv.adapter = adapter
                    }
                    binding.apply {
                        setLeaderData(firstThreeUser[0], tvUserName1, ivUser1, tvUserPoint1)
                        setLeaderData(firstThreeUser[1], tvUserName2, ivUser2, tvUserPoint2)
                        setLeaderData(firstThreeUser[2], tvUserName3, ivUser3, tvUserPoint3)
                    }
                }
            }
        }
    }

    override fun initAction() {
        binding.apply {
            layoutVocabulary.click {
                openFragment(VocabularyFragment::class.java, null, true, R.id.fr_app)
            }
            layoutGrammar.click {
                openFragment(GrammarFragment::class.java, null, true, R.id.fr_app)
            }
            layoutQuote.click {
                openFragment(QuoteFragment::class.java, null, true, R.id.fr_app)
            }
            layoutStory.click {
                openFragment(StoryFragment::class.java, null, true, R.id.fr_app)
            }
            layoutTrivia.click {
                openFragment(TriviaCategoryFragment::class.java, createBundle(USER, user), true, R.id.fr_app)
            }
            layoutQuiz.click {
                openFragment(QuizFragment::class.java, null, true, R.id.fr_app)
            }
            layoutPractice.click {
                openFragment(ExerciseFragment::class.java, null, true, R.id.fr_app)
            }
            layoutFavorite.click {
                openFragment(BookMarkFragment::class.java, null, true, R.id.fr_app)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setLeaderData(user: User, name: TextView, img: ImageView, exp: TextView) {
         name.text = user.userName
        if(user.image.isNotEmpty()) {
            Glide.with(requireContext()).load(user.image).into(img)
        } else {
            img.setImageResource(R.drawable.default_img_user)
        }
        exp.text = "${user.exp} exp"
    }

}