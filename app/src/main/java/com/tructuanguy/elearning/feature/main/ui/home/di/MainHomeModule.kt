package com.tructuanguy.elearning.feature.main.ui.home.di

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.tructuanguy.elearning.di.FirebaseModule
import com.tructuanguy.elearning.feature.main.ui.home.domain.data.MainHomeRepositoryImpl
import com.tructuanguy.elearning.feature.main.ui.home.domain.repository.MainHomeRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MainHomeModule {

    @Provides
    @Singleton
    fun provideMainHomeRepository(
        @FirebaseModule.UserCollection userCollection: CollectionReference
    ): MainHomeRepository {
        return MainHomeRepositoryImpl(userCollection)
    }

}