package com.tructuanguy.elearning.feature.main.ui.me.setting.ui.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.databinding.ItemSettingNextBinding
import com.tructuanguy.elearning.extension.gone
import com.tructuanguy.elearning.extension.visible
import com.tructuanguy.elearning.feature.main.ui.me.setting.ui.model.SettingItem


class SettingAdapter(
    private val listItem: ArrayList<SettingItem>,
    private val listener: (SettingItem, Int) -> Unit
): RecyclerView.Adapter<SettingAdapter.SettingHolder>() {

    inner class SettingHolder(private val binding: ItemSettingNextBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: SettingItem) {
            binding.apply {
                when(item.type) {
                    1 -> next.visible()
                    2 -> {
                        next.gone()
                        featureName.setTextColor(itemView.context.resources.getColor(R.color.red_setting, null))
                    }
                }
                featureName.text = item.name
                icFeature.setImageResource(item.icon)
            }
            itemView.setOnClickListener {
                listener.invoke(item, adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingHolder {
        return SettingHolder(ItemSettingNextBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: SettingHolder, position: Int) {
        holder.bind(listItem[position])
    }

}