package com.tructuanguy.elearning.feature.main.ui.grammar

import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.databinding.FragmentChildGrammarBinding
import com.tructuanguy.elearning.db.entities.Grammar
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.ui.grammar.adapter.ChildGrammarAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChildGrammarFragment : BaseFragmentWithBinding<FragmentChildGrammarBinding>() {

    private val grammarViewModel: GrammarViewModel by viewModels()
    private lateinit var grammar: Grammar

    override fun getViewBinding(inflater: LayoutInflater): FragmentChildGrammarBinding {
        return FragmentChildGrammarBinding.inflate(layoutInflater)
    }

    override fun init() {
        grammar = arguments?.getSerializable(GRAMMAR_PARENT) as Grammar
        binding.textView3.text = grammar.mean
        grammarViewModel.getChildGrammar()
    }

    override fun initData() {
        grammarViewModel.getChildGrammarState.observe(viewLifecycleOwner) {
            when(it) {
                is DataState.Error -> {
                    Log.i("error", it.message)
                }
                is DataState.Loading -> {

                }
                is DataState.Success -> {
                    Log.i("success", it.data.toString())
                    binding.rcvChildGrammar.adapter = ChildGrammarAdapter(it.data) {
                        openFragment(
                            DetailGrammarFragment::class.java,
                            createBundle(GRAMMAR_PARENT, it),
                            true,
                            R.id.fr_app
                        )
                    }
                }
            }
        }
    }

    override fun initAction() {
        binding.icBack.click { onBackPressed() }
    }


}