package com.tructuanguy.elearning.feature.main.ui.home.domain.data

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.di.FirebaseModule
import com.tructuanguy.elearning.feature.main.ui.home.domain.repository.MainHomeRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class MainHomeRepositoryImpl @Inject constructor(
    @FirebaseModule.UserCollection private val userCollection: CollectionReference
) : MainHomeRepository {
    override fun getAllUser(): Flow<DataState<List<User>>> {
        return flow {
            emit(DataState.Loading())
            val result = suspendCoroutine<DataState<List<User>>> { continuation ->
                try {
                    val list = arrayListOf<User>()
                    userCollection.get().addOnCompleteListener { task ->
                        if(task.isSuccessful) {
                            for(document in task.result) {
                                list.add(document.toObject(User::class.java))
                                list.sortByDescending {
                                    user -> user.exp
                                }
                            }
                            continuation.resume(DataState.Success(list))
                        }
                    }
                } catch (e: Exception) {
                    continuation.resume(DataState.Error(e.toString()))
                }
            }
            emit(result)
        }
    }

}