package com.tructuanguy.elearning.feature.log.ui.signup

import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.constants.FirebaseConstants.USER
import com.tructuanguy.elearning.databinding.FragmentProfileCreatedBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.log.ui.signin.SignInViewModel
import com.tructuanguy.elearning.feature.main.MainFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileCreatedFragment : BaseFragmentWithBinding<FragmentProfileCreatedBinding>() {

    private val signInViewModel: SignInViewModel by viewModels()
    private lateinit var user: User

    override fun getViewBinding(inflater: LayoutInflater): FragmentProfileCreatedBinding {
        return FragmentProfileCreatedBinding.inflate(layoutInflater)
    }

    override fun init() {
        user = requireArguments().getSerializable(USER) as User
    }

    override fun initData() {
//        signInViewModel.signInState.observe(viewLifecycleOwner) {
//            when(it) {
//                is DataState.Success<Boolean> -> {
//                    signInViewModel.getUserData()
//                }
//                is DataState.Error -> {
//                    Log.i("error_signin", it.message)
//                }
//                is DataState.Loading -> {
//
//                }
//            }
//            signInViewModel.getUserDataState.observe(viewLifecycleOwner) { state ->
//                when(state) {
//                    is DataState.Success<User> -> {
//                        Handler(Looper.getMainLooper()).postDelayed({
//                            hideLoadingDialog()
//                            val uid = state.data.toString()
//                            val bundle = Bundle()
//                            bundle.putSerializable(UID, uid)
//                            openFragment(MainFragment::class.java, bundle, false)
//                        }, 1500)
//                    }
//                    is DataState.Error -> {
//                        hideLoadingDialog()
//                        Log.i("error_get_user", state.message)
//                    }
//                    is DataState.Loading -> {
//                        showLoadingDialog()
//                    }
//                }
//            }
//        }
    }

    override fun initAction() {
        binding.openHome.click {
            val bundle = Bundle()
            bundle.putSerializable(USER, user)
            openFragment(MainFragment::class.java, bundle, false)
        }
    }
}