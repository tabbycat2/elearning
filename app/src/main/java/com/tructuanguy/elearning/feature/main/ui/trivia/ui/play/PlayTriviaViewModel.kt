package com.tructuanguy.elearning.feature.main.ui.trivia.ui.play

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.main.ui.trivia.domain.usecase.GetTriviaCategory
import com.tructuanguy.elearning.feature.main.ui.trivia.domain.usecase.firebase.UpdateUserExp
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.model.TriviaItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PlayTriviaViewModel @Inject constructor(
    private val getTriviaCategoryUseCase: GetTriviaCategory,
    private val updateUserExpUseCase: UpdateUserExp
) : ViewModel() {

    private val _getTriviaCategoryState = MutableLiveData<DataState<List<TriviaItem>>>()
    val getTriviaCategoryState: LiveData<DataState<List<TriviaItem>>>
        get() = _getTriviaCategoryState

    private val _updateUserExpState = MutableLiveData<DataState<Boolean>>()
    val updateUserExpState: LiveData<DataState<Boolean>>
        get() = _updateUserExpState


    fun updateUserExp(id: String, exp: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            updateUserExpUseCase(id, exp).onEach {
                _updateUserExpState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun getTriviaCategory(category: String) {
        viewModelScope.launch(Dispatchers.IO) {
            getTriviaCategoryUseCase(category).onEach {
                _getTriviaCategoryState.value = it
            }.launchIn(viewModelScope)
        }
    }

}