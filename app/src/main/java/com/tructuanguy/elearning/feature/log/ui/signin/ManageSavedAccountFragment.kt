package com.tructuanguy.elearning.feature.log.ui.signin

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.databinding.FragmentManageSavedAccountBinding
import com.tructuanguy.elearning.db.entities.UserLocal
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.extension.gone
import com.tructuanguy.elearning.extension.showSimpleAlertDialog
import com.tructuanguy.elearning.extension.visible
import com.tructuanguy.elearning.feature.log.ui.signin.adapter.SavedAccountAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ManageSavedAccountFragment : BaseFragmentWithBinding<FragmentManageSavedAccountBinding>() {

    private val signInViewModel: SignInViewModel by viewModels()

    override fun getViewBinding(inflater: LayoutInflater): FragmentManageSavedAccountBinding {
        return FragmentManageSavedAccountBinding.inflate(layoutInflater)
    }

    override fun init() {
        binding.icBack.click {
            onBackPressed()
        }
        signInViewModel.getAllUserLocal()
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun initData() {
        signInViewModel.getAllUserLocalState.observe(viewLifecycleOwner) {
            when(it) {
                is DataState.Error -> {
                }
                is DataState.Loading -> {

                }
                is DataState.Success -> {
                    if (it.data.isNotEmpty()) {
                        lateinit var adapter: SavedAccountAdapter
                        val handleClick: (UserLocal, Int) -> Unit = { user, pos ->
                            requireContext().showSimpleAlertDialog(
                                title = getString(R.string.delete_local_account),
                                message = getString(R.string.delete_local_user_msg),
                                negativeMsg = getString(R.string.cancel),
                                positiveMsg = getString(R.string.delete),
                                onNegative = {
                                    // do nothing
                                },
                                onPositive = {
                                    signInViewModel.deleteUserLocal(user.id)
                                    adapter.notifyItemRemoved(pos)
                                }
                            )
                        }
                        adapter = SavedAccountAdapter(it.data, handleClick)
                        binding.rcvSavedAcc.adapter = adapter
                        adapter.notifyDataSetChanged()
                    } else {
                        binding.lnEmpty.visible()
                        binding.layoutEmpty.tvEmpty.text = getString(R.string.no_account_here)
                        binding.rcvSavedAcc.gone()
                    }
                }
            }
        }
        signInViewModel.deleteUserLocalState.observe(viewLifecycleOwner) {
            when(it) {
                is DataState.Error -> {
                }
                is DataState.Loading -> {

                }
                is DataState.Success -> {
                    toast(getString(R.string.delete_successfully))
                }
            }
        }
    }

    override fun initAction() {

    }
}