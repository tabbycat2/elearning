package com.tructuanguy.elearning.feature.log.domain.usecases.signin

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.log.domain.repository.AuthenticationRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LogOutUseCase @Inject constructor(
    private val authenticationRepository: AuthenticationRepository
) {
    suspend operator fun invoke(): Flow<DataState<Boolean>> = authenticationRepository.logOut()
}