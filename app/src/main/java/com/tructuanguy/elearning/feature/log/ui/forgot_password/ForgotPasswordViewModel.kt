package com.tructuanguy.elearning.feature.log.ui.forgot_password

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.error.EmailError
import com.tructuanguy.elearning.extension.isValidEmail
import com.tructuanguy.elearning.feature.log.domain.usecases.password.ForgotPasswordUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ForgotPasswordViewModel @Inject constructor(
    private val forgotPasswordUseCase: ForgotPasswordUseCase,
) : ViewModel() {

    private val _forgotPasswordState: MutableLiveData<DataState<Boolean>> = MutableLiveData()
    val forgotPasswordState: LiveData<DataState<Boolean>>
        get() = _forgotPasswordState

    private val _errorEmailState = MutableLiveData<EmailError>()
    val errorEmailState: LiveData<EmailError>
        get() = _errorEmailState


    fun forgotPassword(email: String) {
        viewModelScope.launch(Dispatchers.IO) {
            forgotPasswordUseCase(email).onEach {
                _forgotPasswordState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun onSubmitEmail(email: String) {
        if (email.isEmpty()) {
            _errorEmailState.value = EmailError.EMPTY
        } else {
            if (!isValidEmail(email)) {
                _errorEmailState.value = EmailError.NO_INVALID
            }
        }
    }

}