package com.tructuanguy.elearning.feature.main.ui.trivia.model.dto

import com.tructuanguy.elearning.feature.main.ui.trivia.model.dto.ByCategory
import com.tructuanguy.elearning.feature.main.ui.trivia.model.dto.ByDifficulty
import com.tructuanguy.elearning.feature.main.ui.trivia.model.dto.ByState
import com.tructuanguy.elearning.feature.main.ui.trivia.model.dto.ByType

data class MetaData(
    val byCategory: ByCategory,
    val byDifficulty: ByDifficulty,
    val byState: ByState,
    val byType: ByType,
    val lastCreated: String,
    val lastReviewed: String
)