package com.tructuanguy.elearning.feature.main.ui.grammar

import android.util.Log
import android.view.LayoutInflater
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.databinding.FragmentDetailGrammarBinding
import com.tructuanguy.elearning.db.entities.Grammar
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.extension.loadHtmlFromAsset

class DetailGrammarFragment : BaseFragmentWithBinding<FragmentDetailGrammarBinding>() {

    private lateinit var grammar: Grammar

    override fun getViewBinding(inflater: LayoutInflater): FragmentDetailGrammarBinding {
        return FragmentDetailGrammarBinding.inflate(layoutInflater)
    }

    override fun init() {
        grammar = arguments?.getSerializable(GRAMMAR_PARENT) as Grammar
        binding.tvGrammarName.text = grammar.mean
        loadHtmlFromAsset(binding.webviewDetail, grammar.id)
    }

    override fun initData() {
    }

    override fun initAction() {
        binding.icBack.click { onBackPressed() }
    }


}