package com.tructuanguy.elearning.feature.main.ui.me.setting.ui.model

data class SettingItem (
    val name: String,
    val icon: Int,
    val type: Int
)