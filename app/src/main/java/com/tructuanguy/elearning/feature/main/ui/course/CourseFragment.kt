package com.tructuanguy.elearning.feature.main.ui.course

import android.view.LayoutInflater
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.databinding.FragmentCourseBinding

class CourseFragment : BaseFragmentWithBinding<FragmentCourseBinding>() {
    override fun getViewBinding(inflater: LayoutInflater): FragmentCourseBinding {
        return FragmentCourseBinding.inflate(layoutInflater)
    }

    override fun init() {

    }

    override fun initData() {

    }

    override fun initAction() {

    }
}