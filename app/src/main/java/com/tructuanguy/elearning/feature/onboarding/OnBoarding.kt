package com.tructuanguy.elearning.feature.onboarding

data class OnBoarding (
    val source: Int,
    val title: String,
    val description: String
)