package com.tructuanguy.elearning.feature.main.ui.me.setting.ui

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.entities.UserLocal
import com.tructuanguy.elearning.di.AppModule
import com.tructuanguy.elearning.db.domain.usecases.user_local.InsertUserLocalUseCase
import com.tructuanguy.elearning.db.domain.usecases.user_local.IsSavedLocalUserUseCase
import com.tructuanguy.elearning.feature.log.domain.usecases.storage.UpdateUserImageUseCase
import com.tructuanguy.elearning.feature.main.ui.me.setting.ui.model.SettingItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingViewModel @Inject constructor(
    private val resourceProvider: AppModule.ResourceProvider,
    private val insertUserLocalUseCase: InsertUserLocalUseCase,
    private val isSavedLocalUserUseCase: IsSavedLocalUserUseCase,
    private val updateUserImageUseCase: UpdateUserImageUseCase
) : ViewModel() {

    val listSettingItem = MutableLiveData<ArrayList<SettingItem>>()
    private val mListSettingItem: ArrayList<SettingItem> = arrayListOf()

    private val _insertUserLocalState = MutableLiveData<DataState<Boolean>>()
    val insertUserLocalState: LiveData<DataState<Boolean>>
        get() = _insertUserLocalState

    private val _isSavedLocalUserState = MutableLiveData<DataState<Boolean>>()
    val isSavedLocalUserState: LiveData<DataState<Boolean>>
        get() = _isSavedLocalUserState

    private val _updateUserImageState = MutableLiveData<DataState<Boolean>>()
    val updateUserImageState: LiveData<DataState<Boolean>>
        get() = _updateUserImageState

    fun checkSavedUserLocal(email: String) {
        viewModelScope.launch(Dispatchers.IO) {
            isSavedLocalUserUseCase(email).onEach {
                _isSavedLocalUserState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun updateUserImage(image: Uri) {
        viewModelScope.launch(Dispatchers.IO) {
            updateUserImageUseCase(image).onEach {
                _updateUserImageState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun insertUserLocal(user: UserLocal) {
        viewModelScope.launch(Dispatchers.IO) {
            insertUserLocalUseCase(user).onEach {
                _insertUserLocalState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun getSettingFeature() {
        mListSettingItem.add(SettingItem(resourceProvider.getString(R.string.change_password), R.drawable.lock, 1))
        mListSettingItem.add(SettingItem(resourceProvider.getString(R.string.notifications), R.drawable.notification, 1))
        mListSettingItem.add(SettingItem(resourceProvider.getString(R.string.term_of_service), R.drawable.term_of_service, 1))
        mListSettingItem.add(SettingItem(resourceProvider.getString(R.string.privacy_policy), R.drawable.privacy, 1))
        mListSettingItem.add(SettingItem(resourceProvider.getString(R.string.help), R.drawable.help, 1))
        mListSettingItem.add(SettingItem(resourceProvider.getString(R.string.log_out), R.drawable.logout, 2))
        mListSettingItem.add(SettingItem(resourceProvider.getString(R.string.delete_account), R.drawable.trash, 2))
        listSettingItem.value = mListSettingItem
    }

}