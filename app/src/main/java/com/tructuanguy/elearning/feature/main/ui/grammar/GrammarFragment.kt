package com.tructuanguy.elearning.feature.main.ui.grammar

import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.databinding.FragmentGrammarBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.ui.grammar.adapter.ParentGrammarAdapter
import dagger.hilt.android.AndroidEntryPoint

const val GRAMMAR_PARENT = "grammar_parent"

@AndroidEntryPoint
class GrammarFragment : BaseFragmentWithBinding<FragmentGrammarBinding>() {

    private val grammarViewModel: GrammarViewModel by viewModels()

    override fun getViewBinding(inflater: LayoutInflater): FragmentGrammarBinding {
        return FragmentGrammarBinding.inflate(layoutInflater)
    }

    override fun init() {
        grammarViewModel.getParentGrammar()
    }

    override fun initData() {
        grammarViewModel.getParentGrammarState.observe(viewLifecycleOwner) {
            when (it) {
                is DataState.Error -> {
                    Log.i("error", it.message)
                }

                is DataState.Loading -> {
                    // do nothing
                }

                is DataState.Success -> {
                    binding.rcvParentGrammar.adapter = ParentGrammarAdapter(it.data) { grammar ->
                        if (grammar.is_parent == 1) {
                            openFragment(
                                ChildGrammarFragment::class.java,
                                createBundle(GRAMMAR_PARENT, grammar),
                                true,
                                R.id.fr_app
                            )
                        } else {
                            openFragment(
                                DetailGrammarFragment::class.java,
                                createBundle(GRAMMAR_PARENT, grammar),
                                true,
                                R.id.fr_app
                            )
                        }
                    }
                }
            }
        }
    }

    override fun initAction() {
        binding.icBack.click { onBackPressed() }
    }


}