package com.tructuanguy.elearning.feature.splash

import androidx.lifecycle.ViewModel
import com.tructuanguy.elearning.constants.PrefsConstants
import com.tructuanguy.elearning.helper.PreferenceHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val mPrefs: PreferenceHelper
) : ViewModel() {

    fun getCurrentEmail(): String {
        return mPrefs.getString(PrefsConstants.USER_EMAIL).toString()
    }

    fun getCurrentPassword(): String {
        return mPrefs.getString(PrefsConstants.USER_PASSWORD).toString()
    }

}