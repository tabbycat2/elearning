package com.tructuanguy.elearning.feature.log.domain.usecases.storage

import android.net.Uri
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.log.domain.repository.AuthenticationRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UpdateUserImageUseCase @Inject constructor(
    private val authenticationRepository: AuthenticationRepository
) {
    suspend operator fun invoke(image: Uri): Flow<DataState<Boolean>> = authenticationRepository.updateUserImage(image)
}