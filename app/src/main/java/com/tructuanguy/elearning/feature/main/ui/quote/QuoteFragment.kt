package com.tructuanguy.elearning.feature.main.ui.quote

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.databinding.FragmentCelebrityBinding
import com.tructuanguy.elearning.extension.click
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class QuoteFragment : BaseFragmentWithBinding<FragmentCelebrityBinding>() {

    private val quoteViewModel: QuoteViewModel by viewModels()

    override fun getViewBinding(inflater: LayoutInflater): FragmentCelebrityBinding {
        return FragmentCelebrityBinding.inflate(layoutInflater)
    }

    override fun init() {
        quoteViewModel.getAllQuote()
    }

    @SuppressLint("SetTextI18n")
    override fun initData() {
        quoteViewModel.getAllQuoteState.observe(viewLifecycleOwner) { state ->
            when (state) {
                is DataState.Error -> {
                }

                is DataState.Loading -> {

                }

                is DataState.Success -> {
                    binding.vpgQuote.adapter = QuoteAdapter(state.data)
                    binding.vpgQuote.registerOnPageChangeCallback(object : OnPageChangeCallback() {
                        override fun onPageSelected(position: Int) {
                            binding.tvCurrentQuote.text =
                                "Quote: ${position + 1}/${state.data.size}"
                        }
                    })
                }
            }
        }
    }

    override fun initAction() {
        binding.icBack.click { onBackPressed() }
        binding.cvNext.click {
            binding.vpgQuote.currentItem = binding.vpgQuote.currentItem + 1
        }
    }


}