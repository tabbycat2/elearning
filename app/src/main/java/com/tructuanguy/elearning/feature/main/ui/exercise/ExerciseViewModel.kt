package com.tructuanguy.elearning.feature.main.ui.exercise

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.usecases.exercise.GetExamQuestion
import com.tructuanguy.elearning.db.domain.usecases.exercise.GetGeneralExeQuestion
import com.tructuanguy.elearning.db.entities.ExamQuestion
import com.tructuanguy.elearning.db.entities.GeneralExeQuestion
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ExerciseViewModel @Inject constructor(
    private val getGeneralExeQuestionUseCase: GetGeneralExeQuestion,
    private val getExamQuestionUseCase: GetExamQuestion
) : ViewModel() {

    private val _getGeneralExeQuestionState = MutableLiveData<DataState<List<GeneralExeQuestion>>>()
    val getGeneralExeQuestionState: LiveData<DataState<List<GeneralExeQuestion>>>
        get() = _getGeneralExeQuestionState

    private val _getExamQuestionState = MutableLiveData<DataState<List<ExamQuestion>>>()
    val getExamQuestionState: LiveData<DataState<List<ExamQuestion>>>
        get() = _getExamQuestionState

    fun getExamQuestion(start: Int, end: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            getExamQuestionUseCase(start, end).onEach {
                _getExamQuestionState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun getGeneralExeQuestion(start: Int, end: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            getGeneralExeQuestionUseCase(start, end).onEach {
                _getGeneralExeQuestionState.value = it
            }.launchIn(viewModelScope)
        }
    }

}