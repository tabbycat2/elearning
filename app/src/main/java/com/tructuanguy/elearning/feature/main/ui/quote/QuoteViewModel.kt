package com.tructuanguy.elearning.feature.main.ui.quote

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.usecases.quote.GetAllQuotes
import com.tructuanguy.elearning.db.entities.Celebrity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QuoteViewModel @Inject constructor(
    private val getAllQuotesUseCase: GetAllQuotes
) : ViewModel() {

    private val _getAllQuoteState = MutableLiveData<DataState<List<Celebrity>>>()
    val getAllQuoteState: LiveData<DataState<List<Celebrity>> >
        get() = _getAllQuoteState

    fun getAllQuote() {
        viewModelScope.launch(Dispatchers.IO) {
            getAllQuotesUseCase().onEach {
                _getAllQuoteState.value = it
            }.launchIn(viewModelScope)
        }
    }

}