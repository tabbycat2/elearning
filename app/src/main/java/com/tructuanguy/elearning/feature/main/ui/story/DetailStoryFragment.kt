package com.tructuanguy.elearning.feature.main.ui.story

import android.text.Html
import android.view.LayoutInflater
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.databinding.FragmentDetailStoryBinding
import com.tructuanguy.elearning.db.entities.Story
import com.tructuanguy.elearning.extension.click

class DetailStoryFragment : BaseFragmentWithBinding<FragmentDetailStoryBinding>() {

    private lateinit var story: Story

    override fun getViewBinding(inflater: LayoutInflater): FragmentDetailStoryBinding {
        return FragmentDetailStoryBinding.inflate(layoutInflater)
    }

    override fun init() {
        story = arguments?.getSerializable(STORY) as Story
        binding.tvDetailStory.text = Html.fromHtml(story.content)
        binding.title.text = story.title
        binding.layoutEnglish.click {
            binding.tvDetailStory.text = ""
            binding.title.text = ""
            binding.title.text = story.title
            binding.tvDetailStory.text = Html.fromHtml(story.content)
        }
        binding.layoutVn.click {
            binding.tvDetailStory.text = ""
            binding.title.text = ""
            binding.title.text = story.description
            binding.tvDetailStory.text = Html.fromHtml(story.mean)
        }
    }

    override fun initData() {

    }

    override fun initAction() {
        binding.icBack.click { onBackPressed() }
    }


}