package com.tructuanguy.elearning.feature.main.ui.boormark

import android.view.LayoutInflater
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.databinding.FragmentBookmarkBinding

class BookMarkFragment : BaseFragmentWithBinding<FragmentBookmarkBinding>() {
    override fun getViewBinding(inflater: LayoutInflater): FragmentBookmarkBinding {
        return FragmentBookmarkBinding.inflate(layoutInflater)
    }

    override fun init() {

    }

    override fun initData() {

    }

    override fun initAction() {

    }


}