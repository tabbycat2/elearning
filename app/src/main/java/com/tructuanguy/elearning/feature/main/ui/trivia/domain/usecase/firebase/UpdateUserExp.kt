package com.tructuanguy.elearning.feature.main.ui.trivia.domain.usecase.firebase

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.main.ui.trivia.domain.repository.TriviaRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UpdateUserExp @Inject constructor(
    private val triviaRepository: TriviaRepository
) {
    suspend operator fun invoke(id: String, exp: Int): Flow<DataState<Boolean>> = triviaRepository.updateUserExp(id, exp)
}