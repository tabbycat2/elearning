package com.tructuanguy.elearning.feature.main.ui.trivia.model.dto

data class ByType(
    val image_choice: Int,
    val text_choice: Int
)