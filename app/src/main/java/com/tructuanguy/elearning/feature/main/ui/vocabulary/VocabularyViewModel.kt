package com.tructuanguy.elearning.feature.main.ui.vocabulary

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.db.domain.usecases.vocabulary.GetAllVocabularyTopicUseCase
import com.tructuanguy.elearning.db.domain.usecases.vocabulary.GetWordByTopicId
import com.tructuanguy.elearning.db.entities.Word
import com.tructuanguy.elearning.feature.main.model.Topic
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class VocabularyViewModel @Inject constructor(
    private val getAllVocabularyTopicUseCase: GetAllVocabularyTopicUseCase,
    private val getWordByTopicIdUseCase: GetWordByTopicId
): ViewModel() {

    private val _getAllTopicState = MutableLiveData<DataState<List<Topic>>>()
    val getAllTopicState: LiveData<DataState<List<Topic>>>
        get() = _getAllTopicState

    private val _getWordByTopicIdState = MutableLiveData<DataState<List<Word>>>()
    val getWordByTopicIdState: LiveData<DataState<List<Word>>>
        get() = _getWordByTopicIdState

    fun getWordByTopicId(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            getWordByTopicIdUseCase(id).onEach {
                _getWordByTopicIdState.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun getAllTopic() {
        viewModelScope.launch(Dispatchers.IO) {
            getAllVocabularyTopicUseCase().onEach {
                _getAllTopicState.value = it
            }.launchIn(viewModelScope)
        }
    }

}