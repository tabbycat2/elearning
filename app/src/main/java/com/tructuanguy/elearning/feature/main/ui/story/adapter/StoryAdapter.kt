package com.tructuanguy.elearning.feature.main.ui.story.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.elearning.databinding.ItemStoryBinding
import com.tructuanguy.elearning.db.entities.Story
import com.tructuanguy.elearning.extension.click

class StoryAdapter(
    private val list: List<Story>,
    private val listener: (Story, Int) -> Unit
): RecyclerView.Adapter<StoryAdapter.StoryHolder>() {

    inner class StoryHolder(private val binding: ItemStoryBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(story: Story) {
            binding.tvIndex.text = (adapterPosition + 1).toString()
            binding.tvTitle.text = story.title
            binding.tvDescription.text = story.description
            itemView.click {
                listener.invoke(story, adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoryHolder {
        return StoryHolder(ItemStoryBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: StoryHolder, position: Int) {
        holder.bind(list[position])
    }

}