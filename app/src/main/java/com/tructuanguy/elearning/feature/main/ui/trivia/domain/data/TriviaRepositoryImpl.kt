package com.tructuanguy.elearning.feature.main.ui.trivia.domain.data

import com.google.firebase.firestore.CollectionReference
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.di.FirebaseModule
import com.tructuanguy.elearning.feature.main.ui.trivia.domain.repository.TriviaRepository
import com.tructuanguy.elearning.feature.main.ui.trivia.model.api.TriviaApi
import com.tructuanguy.elearning.feature.main.ui.trivia.model.dto.MetaData
import com.tructuanguy.elearning.feature.main.ui.trivia.ui.model.TriviaItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class TriviaRepositoryImpl @Inject constructor(
    private val api: TriviaApi,
    @FirebaseModule.UserCollection private val userCollection: CollectionReference
) : TriviaRepository {
    override suspend fun getMetaData(): Flow<DataState<MetaData>> {
        return flow {
            emit(DataState.Loading())
            try {
                val metaData = api.getMetaData()
                emit(DataState.Success(metaData))
            } catch (e: HttpException) {
                emit(DataState.Error(e.localizedMessage!! ?: "An expected error occur"))
            } catch (e: IOException) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun getCategoryQuiz(category: String): Flow<DataState<List<TriviaItem>>> {
        return flow {
            try {
                val listQuizItem = api.getCategoryQuestion(category)
                emit(DataState.Success(listQuizItem))
            } catch (e: HttpException) {
                emit(DataState.Error(e.localizedMessage!! ?: "An expected error occur"))
            } catch (e: IOException) {
                emit(DataState.Error(e.toString()))
            }
        }
    }

    override suspend fun updateUserExp(id: String, exp: Int): Flow<DataState<Boolean>> {
         return flow {
             emit(DataState.Loading())
             val result = suspendCoroutine<DataState<Boolean>> { continuation ->
                 try {
                     userCollection.document(id).update("exp", exp)
                         .addOnSuccessListener {
                             continuation.resume(DataState.Success(true))
                         }
                         .addOnFailureListener {
                             continuation.resume(DataState.Error(it.toString()))
                         }
                 } catch (e: Exception) {
                    continuation.resume(DataState.Error(e.toString()))
                 }
             }
             emit(result)
         }
    }


}