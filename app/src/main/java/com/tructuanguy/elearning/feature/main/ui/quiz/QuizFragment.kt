package com.tructuanguy.elearning.feature.main.ui.quiz

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.AdapterView
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.databinding.FragmentQuizBinding
import com.tructuanguy.elearning.db.entities.Word
import com.tructuanguy.elearning.extension.click
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class QuizFragment : BaseFragmentWithBinding<FragmentQuizBinding>() {

    private val quizViewModel: QuizViewModel by viewModels()
    private var listenersInitialized = false
    private var arrWord: ArrayList<Word> = arrayListOf()
    private var word: String = ""
    private var arrCharFromWord: ArrayList<String> = arrayListOf()
    private var arrEmptyFromWord: ArrayList<String> = arrayListOf()
    private var currentPos = 0
    private var index = 0

    override fun getViewBinding(inflater: LayoutInflater): FragmentQuizBinding {
        return FragmentQuizBinding.inflate(layoutInflater)
    }

    override fun init() {
        quizViewModel.getAllWord()
    }

    override fun initData() {
        quizViewModel.getAllWordState.observe(viewLifecycleOwner) { state ->
            when (state) {
                is DataState.Error -> {
                    Log.i("error", state.message)
                }

                is DataState.Loading -> {

                }

                is DataState.Success -> {
                    val list = state.data
                    list.shuffled()
                    for (i in list.indices) {
                        arrWord.add(list[i])
                    }
                    arrWord.shuffle()
                    word = arrWord[currentPos].word
                    arrCharFromWord = getCharsFromWord(word)
                    Glide.with(requireContext()).load(arrWord[currentPos].image).into(binding.ivSubContent)
                    arrEmptyFromWord = getGridAnswer(word)
                    Glide.with(requireContext()).load(word)
                    binding.gridData.adapter = GridDataAdapter(requireContext(), arrCharFromWord, 2)
                    binding.gridAnswer.adapter =
                        GridDataAdapter(requireContext(), arrEmptyFromWord, 1)
                    play()
                }
            }
        }
    }

    private fun play() {
        toast("play")
        if (!listenersInitialized) {
            binding.gridData.onItemClickListener =
                AdapterView.OnItemClickListener { parent, _, pos, _ ->
                    val s = parent.getItemAtPosition(pos) as String
                    if (s.isNotEmpty() && index < arrEmptyFromWord.size) {
                        for (i in 0 until arrEmptyFromWord.size step 1) {
                            if (arrEmptyFromWord[i].isEmpty()) {
                                index = i
                                break
                            }
                        }
                        arrCharFromWord[pos] = ""
                        arrEmptyFromWord[index] = s
                        index++
                        binding.gridData.adapter =
                            GridDataAdapter(requireContext(), arrCharFromWord, 2)
                        binding.gridAnswer.adapter =
                            GridDataAdapter(requireContext(), arrEmptyFromWord, 1)
                        if (index == arrEmptyFromWord.size) {
                            checkWin()
                        }
                    }
                }
            binding.gridAnswer.onItemClickListener =
                AdapterView.OnItemClickListener { parent, _, pos, _ ->
                    val s: String = parent.getItemAtPosition(pos) as String
                    if (s.isNotEmpty()) {
                        index = pos
                        arrEmptyFromWord[pos] = ""
                        for (i in 0 until arrEmptyFromWord.size step 1) {
                            if (arrCharFromWord[i].isEmpty()) {
                                arrCharFromWord[i] = s
                                break
                            }
                        }
                        binding.gridData.adapter =
                            GridDataAdapter(requireContext(), arrCharFromWord, 2)
                        binding.gridAnswer.adapter =
                            GridDataAdapter(requireContext(), arrEmptyFromWord, 1)
                    }
                }
            listenersInitialized = true
        }
    }

    private fun checkWin() {
        Log.i("!!!", word)
        var s = ""
        for (i in arrEmptyFromWord.indices step 1) {
            s += arrEmptyFromWord[i]
        }
        if (s.uppercase() == word.uppercase()) {
            showDetailDialog(arrWord[currentPos])
        } else {
            toast("false")
        }
    }

    private fun showDetailDialog(word: Word) {
        val bundle = Bundle()
        bundle.putSerializable("quiz_word", word)
        val dialog = DetailWordDialog {
            nextQuestion()
        }
        dialog.arguments = bundle
        dialog.show(childFragmentManager, this@QuizFragment.javaClass.simpleName)
    }

    private fun nextQuestion() {
        currentPos++
        word = arrWord[currentPos].word
        index = 0
        Glide.with(requireContext()).load(arrWord[currentPos].image).into(binding.ivSubContent)
        arrCharFromWord = getCharsFromWord(word)
        arrEmptyFromWord = getGridAnswer(word)
        binding.gridData.adapter =
            GridDataAdapter(requireContext(), arrCharFromWord, 2)
        binding.gridAnswer.adapter =
            GridDataAdapter(requireContext(), arrEmptyFromWord, 1)
        play()
    }


    private fun getGridAnswer(word: String): ArrayList<String> {
        val list = arrayListOf<String>()
        for (i in word.indices step 1) {
            list.add("")
        }
        return list
    }

    private fun getCharsFromWord(word: String): ArrayList<String> {
        val list = arrayListOf<String>()
        for (i in word.indices step 1) {
            list.add(word[i].toString())
        }
        list.shuffle()
        return list
    }

    override fun initAction() {
        binding.icBack.click { onBackPressed() }
    }

}