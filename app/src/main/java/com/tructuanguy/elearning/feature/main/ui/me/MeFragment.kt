package com.tructuanguy.elearning.feature.main.ui.me

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import com.bumptech.glide.Glide
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.User
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.constants.FirebaseConstants.USER
import com.tructuanguy.elearning.databinding.FragmentMeBinding
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.ui.me.setting.ui.SettingFragment

class MeFragment : BaseFragmentWithBinding<FragmentMeBinding>() {

    private lateinit var user: User

    override fun getViewBinding(inflater: LayoutInflater): FragmentMeBinding {
        return FragmentMeBinding.inflate(layoutInflater)
    }

    @SuppressLint("SetTextI18n")
    override fun init() {
        user = arguments?.getSerializable(USER) as User
        if(user.image.isNotEmpty()) {
            Glide.with(requireContext()).load(user.image).into(binding.avatar)
        } else {
            binding.avatar.setImageResource(R.drawable.default_img_user)
        }
        binding.userExp.text = "${user.exp} exp"
    }

    override fun initData() {
    }

    override fun initAction() {
        binding.setting.click {
            val bundle = Bundle()
            bundle.putSerializable(USER, user)
            openFragment(SettingFragment::class.java, bundle, true, R.id.fr_app)
        }
        binding.icBack.click { onBackPressed() }
    }
}