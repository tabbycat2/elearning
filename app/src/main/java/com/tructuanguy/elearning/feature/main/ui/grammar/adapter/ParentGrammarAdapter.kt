package com.tructuanguy.elearning.feature.main.ui.grammar.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.elearning.databinding.ItemGrammarParentBinding
import com.tructuanguy.elearning.db.entities.Grammar
import com.tructuanguy.elearning.extension.click

class ParentGrammarAdapter(
    private val list: List<Grammar>,
    private val listener: (Grammar) -> Unit
) : RecyclerView.Adapter<ParentGrammarAdapter.ParentGrammarHolder>() {

    inner class ParentGrammarHolder(private val binding: ItemGrammarParentBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(grammar: Grammar) {
            binding.tvIndex.text = (adapterPosition + 1).toString()
            binding.tvGrammarName.text = grammar.mean
            itemView.click {
                listener.invoke(grammar)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentGrammarHolder {
        return ParentGrammarHolder(ItemGrammarParentBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ParentGrammarHolder, position: Int) {
        holder.bind(list[position])
    }


}