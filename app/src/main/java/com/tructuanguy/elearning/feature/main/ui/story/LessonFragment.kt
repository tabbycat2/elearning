package com.tructuanguy.elearning.feature.main.ui.story

import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.base.BaseFragmentWithBinding
import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.databinding.FragmentLessonBinding
import com.tructuanguy.elearning.db.entities.Lesson
import com.tructuanguy.elearning.extension.click
import com.tructuanguy.elearning.feature.main.ui.story.adapter.StoryAdapter
import dagger.hilt.android.AndroidEntryPoint

const val STORY = "story"

@AndroidEntryPoint
class LessonFragment : BaseFragmentWithBinding<FragmentLessonBinding>() {

    private val storyViewModel: StoryViewModel by viewModels()
    private lateinit var lesson: Lesson

    override fun getViewBinding(inflater: LayoutInflater): FragmentLessonBinding {
        return FragmentLessonBinding.inflate(layoutInflater)
    }

    override fun init() {
        lesson = arguments?.getSerializable(LESSON) as Lesson
        binding.title.text = lesson.name
        storyViewModel.getStory(lesson.id + 1)
    }

    override fun initData() {
        storyViewModel.getStoryState.observe(viewLifecycleOwner) {
            when(it) {
                is DataState.Error -> {
                    Log.i("error", it.message)
                }
                is DataState.Loading -> {

                }
                is DataState.Success -> {
                    binding.rcvStory.adapter = StoryAdapter(it.data) { story, _ ->
                        openFragment(
                            DetailStoryFragment::class.java,
                            createBundle(STORY, story),
                            true,
                            R.id.fr_app
                        )
                    }
                }
            }
        }
    }

    override fun initAction() {
        binding.icBack.click { onBackPressed() }
    }


}