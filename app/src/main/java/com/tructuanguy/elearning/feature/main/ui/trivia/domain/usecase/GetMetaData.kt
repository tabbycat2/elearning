package com.tructuanguy.elearning.feature.main.ui.trivia.domain.usecase

import com.tructuanguy.elearning.common.DataState
import com.tructuanguy.elearning.feature.main.ui.trivia.domain.repository.TriviaRepository
import com.tructuanguy.elearning.feature.main.ui.trivia.model.dto.MetaData
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMetaData @Inject constructor(
    private val triviaRepository: TriviaRepository
) {
    suspend operator fun invoke(): Flow<DataState<MetaData>> = triviaRepository.getMetaData()
}