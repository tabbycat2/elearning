package com.tructuanguy.elearning.feature.log.ui.signin.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tructuanguy.elearning.databinding.ItemAccountSavedBinding
import com.tructuanguy.elearning.db.entities.UserLocal

class SavedAccountAdapter(
    private val list: List<UserLocal>,
    private val listener: (UserLocal, Int) -> Unit
) : RecyclerView.Adapter<SavedAccountAdapter.SavedAccountHolder>() {

    inner class SavedAccountHolder(private val binding: ItemAccountSavedBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(user: UserLocal) {
            binding.apply {
                tvUser.text = user.name
            }
            itemView.setOnClickListener {
                listener.invoke(user, adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedAccountHolder {
        return SavedAccountHolder(ItemAccountSavedBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SavedAccountHolder, position: Int) {
        holder.bind(list[position])
    }


}