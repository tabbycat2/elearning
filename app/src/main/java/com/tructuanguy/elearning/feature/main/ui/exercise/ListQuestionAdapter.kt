package com.tructuanguy.elearning.feature.main.ui.exercise

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.tructuanguy.elearning.R
import com.tructuanguy.elearning.db.entities.ExamQuestion

class ListQuestionAdapter(context: Context, arrQuestion: List<ExamQuestion>) : ArrayAdapter<ExamQuestion>(context, 0, arrQuestion) {

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_question, parent, false)
        }

        val tvItemQuesNum = view?.findViewById<TextView>(R.id.tvItemQuesNum)
        val tvItemQuesChoice = view?.findViewById<TextView>(R.id.tvItemQuesChoice)
        val tvItemQuesTrue = view?.findViewById<TextView>(R.id.tvItemQuesTrue)

        val question = getItem(position)

        if (question != null) {
            tvItemQuesNum?.text = "${position + 1}"
            tvItemQuesChoice?.text = getChoice(question.t_answer)

            if (DetailExerciseFragment.checkResult) {
                tvItemQuesTrue?.visibility = View.VISIBLE
                tvItemQuesTrue?.text = getChoice(question.t_key!!)
                tvItemQuesChoice?.setBackgroundResource(R.drawable.circle_index_orange)
                tvItemQuesTrue?.setBackgroundResource(R.drawable.circle_index_green)
                if (question.t_answer == question.t_key) {
                    tvItemQuesChoice?.setBackgroundResource(R.drawable.circle_index_green)
                }
            }
        }

        return view!!
    }

    private fun getChoice(choice: Int): String {
        return if (choice == 1) "A" else if (choice == 2) "B" else if (choice == 3) "C" else if (choice == 4) "D" else "-"
    }

}
