package com.tructuanguy.elearning.feature.main.ui.trivia.model.dto

data class ByDifficulty(
    val easy: Int,
    val hard: Int,
    val medium: Int
)