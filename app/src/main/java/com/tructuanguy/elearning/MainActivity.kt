package com.tructuanguy.elearning


import android.view.LayoutInflater
import com.tructuanguy.elearning.base.BaseActivity
import com.tructuanguy.elearning.databinding.ActivityMainBinding
import com.tructuanguy.elearning.feature.splash.SplashFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {
    override fun getViewBinding(inflater: LayoutInflater): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    override fun init() {
        openFragment(SplashFragment::class.java, null, false)
    }


}